/* Copyright (C) 2013-2014 TU Dortmund
 * This file is part of LearnLib, http://www.learnlib.de/.
 * 
 * LearnLib is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License version 3.0 as published by the Free Software Foundation.
 * 
 * LearnLib is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with LearnLib; if not, see
 * <http://www.gnu.de/documents/lgpl.en.html>.
 */
package de.learnlib.cache.dfa;

import de.learnlib.algorithms.ttt.experiments.statistics.WordSerializer;
import de.learnlib.algorithms.ttt.experiments.tools.SequenceBuilder;
import de.learnlib.api.MembershipOracle;
import de.learnlib.api.Query;
import de.learnlib.cache.LearningCacheOracle;
import de.learnlib.oracles.DefaultQuery;
import net.automatalib.incremental.dfa.Acceptance;
import net.automatalib.incremental.dfa.IncrementalDFABuilder;
import net.automatalib.incremental.dfa.dag.IncrementalDFADAGBuilder;
import net.automatalib.incremental.dfa.tree.IncrementalDFATreeBuilder;
import net.automatalib.words.Alphabet;
import net.automatalib.words.Word;

import javax.annotation.ParametersAreNonnullByDefault;
import java.math.BigInteger;
import java.util.*;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;


/**
 * DFA cache. This cache is implemented as a membership oracle: upon construction, it is
 * provided with a delegate oracle. Queries that can be answered from the cache are answered
 * directly, others are forwarded to the delegate oracle. When the delegate oracle has finished
 * processing these remaining queries, the results are incorporated into the cache.
 *
 * @param <I> input symbol class
 * @author Malte Isberner
 */
@ParametersAreNonnullByDefault
public class DFAFillCacheOracle<I> implements LearningCacheOracle.DFALearningCacheOracle<I> {


    private final IncrementalDFABuilder<I> incDfa;
    private final Lock incDfaLock;
    private final MembershipOracle<I, Boolean> delegate;
    private final int minBatchSize;
    private final SequenceBuilder<I> sequenceBuilder;
    private final List<BigInteger> injectedWords;
    /**
     * Constructor.
     *
     * @param alphabet the alphabet of the cache
     * @param delegate the delegate oracle
     * @deprecated since 2014-01-24. Use {@link DFACaches#createCache(Alphabet, MembershipOracle)}
     */
    @Deprecated
    public DFAFillCacheOracle(Alphabet<I> alphabet, MembershipOracle<I, Boolean> delegate, int minBatchSize) {
        this(new IncrementalDFADAGBuilder<>(alphabet), delegate, minBatchSize);
    }
    public DFAFillCacheOracle(IncrementalDFABuilder<I> incDfa, MembershipOracle<I, Boolean> delegate, int minBatchSize) {
        this(incDfa, new ReentrantLock(), delegate, minBatchSize);
    }

    private DFAFillCacheOracle(IncrementalDFABuilder<I> incDfa, Lock lock, MembershipOracle<I, Boolean> delegate, int minBatchSize) {
        this.incDfa = incDfa;
        this.incDfaLock = lock;
        this.delegate = delegate;
        this.minBatchSize = minBatchSize;
        sequenceBuilder = new SequenceBuilder<>(incDfa.getInputAlphabet());
        injectedWords = new ArrayList<>();
    }

    public static <I>
    DFAFillCacheOracle<I> createTreeCacheOracle(Alphabet<I> alphabet, MembershipOracle<I, Boolean> delegate, int minBatchSize) {
        return new DFAFillCacheOracle<>(new IncrementalDFATreeBuilder<>(alphabet), delegate, minBatchSize);
    }

    public static <I>
    DFAFillCacheOracle<I> createDAGCacheOracle(Alphabet<I> alphabet, MembershipOracle<I, Boolean> delegate, int minBatchSize) {
        return new DFAFillCacheOracle<>(new IncrementalDFADAGBuilder<>(alphabet), delegate, minBatchSize);
    }

    /**
     * Creates an equivalence oracle that checks an hypothesis for consistency with the
     * contents of this cache. Note that the returned oracle is backed by the cache data structure,
     * i.e., it is sufficient to call this method once after creation of the cache.
     *
     * @return the cache consistency test backed by the contents of this cache.
     */
    @Override
    public DFACacheConsistencyTest<I> createCacheConsistencyTest() {
        return new DFACacheConsistencyTest<>(incDfa, incDfaLock);
    }

    /*
     * (non-Javadoc)
     * @see de.learnlib.api.MembershipOracle#processQueries(java.util.Collection)
     */
    @Override
    public void processQueries(Collection<? extends Query<I, Boolean>> queries) {
        List<Query<I, Boolean>> unanswered = new ArrayList<>();
        Set<Word<I>> unanseredWord = new HashSet<>();
        List<DefaultQuery<I, Boolean>> toAnswer = new ArrayList<>();

        incDfaLock.lock();
        try {
            for (Query<I, Boolean> q : queries) {

                Acceptance acc = incDfa.lookup(q.getInput());
                if (acc != Acceptance.DONT_KNOW) {
                    q.answer(acc.toBoolean());
                } else {
                    unanseredWord.add(q.getInput());
                    unanswered.add(q);
                }
            }
            for (Word<I> word : unanseredWord) {
                toAnswer.add(new DefaultQuery<>(word));
            }
            if (toAnswer.size() > 0) {
                int targetSize = Math.max((toAnswer.size() / minBatchSize + 1) * minBatchSize, minBatchSize);
                while (toAnswer.size() < targetSize) {
                    Word<I> word = sequenceBuilder.next();
                    if (incDfa.lookup(word) == Acceptance.DONT_KNOW && !unanseredWord.contains(word)) {
                        DefaultQuery<I, Boolean> query = new DefaultQuery<>(word);
                        toAnswer.add(query);
                        injectedWords.add(WordSerializer.wordToBigint(incDfa.getInputAlphabet(),word));
                    }
                }
            }
        } finally {
            incDfaLock.unlock();
        }

        delegate.processQueries(toAnswer);

        incDfaLock.lock();
        try {
            for (DefaultQuery<I, Boolean> q : toAnswer) {
                incDfa.insert(q.getInput(), q.getOutput());
            }
            for (Query<I, Boolean> q : unanswered) {
                Acceptance acc = incDfa.lookup(q.getInput());
                if (acc != Acceptance.DONT_KNOW)
                    q.answer(acc.toBoolean());
                else System.err.format("Unanswered Word: %s\n", q.getInput());
            }
        } finally {
            incDfaLock.unlock();
        }
    }

    public Boolean lookup(DefaultQuery<I,Boolean> q){
        Acceptance acc = incDfa.lookup(q.getInput());
        if(acc==Acceptance.DONT_KNOW) return false;
        q.answer(acc.toBoolean());
        return true;
    }


    public List<BigInteger> getInjectedWords(){return injectedWords;}
}
