package de.learnlib.algorithms.ttt.experiments.statistics;

import de.learnlib.api.MembershipOracle;
import de.learnlib.api.Query;
import de.learnlib.oracles.DefaultQuery;
import net.automatalib.words.Word;

import java.util.Collection;

/**
 * Created by Hongzhi Wang on 2015/5/19.
 */
public class ForkOracle<I,D> implements MembershipOracle<I,D> {
    private final Collection<MembershipOracle<I,D>> oracles;
    public ForkOracle(Collection<MembershipOracle<I,D>> oracles){
        this.oracles=oracles;
    }

    @Override
    public void processQueries(Collection<? extends Query<I, D>> queries) {
        for(MembershipOracle oracle:oracles)
            oracle.processQueries(queries);
    }

    @Override
    public void processQuery(Query<I, D> query) {
        for(MembershipOracle oracle:oracles)
            oracle.processQuery(query);
    }

    @Override
    public D answerQuery(Word<I> input) {
        DefaultQuery<I,D> query=new DefaultQuery<I, D>(input,Word.<I>epsilon());
        processQuery(query);
        return query.getOutput();
    }

    @Override
    public D answerQuery(Word<I> prefix, Word<I> suffix) {
        DefaultQuery<I,D> query=new DefaultQuery<I, D>(prefix,suffix);
        processQuery(query);
        return query.getOutput();
    }

    @Override
    public MembershipOracle<I, D> asOracle() {
        return this;
    }
}
