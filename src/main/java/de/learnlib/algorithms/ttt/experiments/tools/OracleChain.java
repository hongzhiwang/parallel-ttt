package de.learnlib.algorithms.ttt.experiments.tools;

import de.learnlib.algorithms.ttt.experiments.statistics.ForkOracle;
import de.learnlib.algorithms.ttt.experiments.statistics.QueryCounter;
import de.learnlib.algorithms.ttt.experiments.statistics.StatisticOracle;
import de.learnlib.api.MembershipOracle;
import de.learnlib.cache.dfa.DFACaches;
import de.learnlib.cache.dfa.DFAFillCacheOracle;
import de.learnlib.cache.mealy.MealyCaches;
import de.learnlib.oracles.SimulatorOracle;
import de.learnlib.parallelism.ParallelOracle;
import de.learnlib.parallelism.StaticParallelOracle;
import net.automatalib.automata.Automaton;
import net.automatalib.automata.fsa.DFA;
import net.automatalib.automata.transout.MealyMachine;
import net.automatalib.automata.transout.impl.compact.CompactMealy;
import net.automatalib.words.Alphabet;
import net.automatalib.words.Word;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

public abstract class OracleChain<A extends Automaton, I, D> {
    final protected int minWorkerBatchSize = 1;
    final protected int minBatchSize=8;
    final protected Alphabet<I> alphabet;
    protected int numWorkers;
    protected CachePolicy cachePolicy;
    protected StatisticOracle<I, D> topStat;
    protected ForkOracle<I, D> forkOracle;
    protected MembershipOracle<I, D> cacheOracle;
    protected StatisticOracle<I, D> parallelStat;
    protected ParallelOracle<I, D> parallelOracle;
    protected List<StatisticOracle<I, D>> workers;
    protected ParallelOracle<I, D> shadowOracle;
    protected List<StatisticOracle<I, D>> shadowWorkers;
    protected A target;

    public OracleChain(int numWorkers, CachePolicy cachePolicy, A target, Alphabet<I> alphabet) {
        this.cachePolicy = cachePolicy;
        this.numWorkers = numWorkers;
        this.target = target;
        this.alphabet=alphabet;
        this.workers=new ArrayList<>(numWorkers);
        this.shadowWorkers=new ArrayList<>(numWorkers);
        createOracles();
    }

    protected abstract void createOracles();

    public Alphabet<I> getInputAlphabet(){return alphabet;}

    public void shutdown() {
        parallelOracle.shutdown();
        shadowOracle.shutdown();
    }

    public MembershipOracle getTopOracle() {
        return topStat;
    }

    public QueryCounter getTopCounter() {
        return topStat.getCounter();
    }

    public QueryCounter getParallelCounter() {
        return parallelStat.getCounter();
    }

    public List<QueryCounter> getWorkerCounters() {
        List<QueryCounter> counters = new ArrayList<>(workers.size());
        for (StatisticOracle so : workers) {
            counters.add(so.getCounter());
        }

        return counters;
    }

    public List<QueryCounter> getShadowCounters() {
        List<QueryCounter> counters = new ArrayList<>(shadowWorkers.size());
        for (StatisticOracle so : shadowWorkers) {
            counters.add(so.getCounter());
        }
        return counters;
    }

    public A getTarget() {
        return target;
    }



    public static enum CachePolicy {
        NormalCache,
        FillCache
    }

    public static class DFAOracleChain<I> extends OracleChain<DFA<?, I>, I, Boolean> {

        public DFAOracleChain(int numWorkers, CachePolicy cachePolicy, DFA<?,I> target, Alphabet<I> alphabet) {
            super(numWorkers, cachePolicy, target, alphabet);
        }

        public List<BigInteger> getCacheInjectedWords(){
            if (cachePolicy!=CachePolicy.FillCache) return new ArrayList<>();
            return ((DFAFillCacheOracle<I>) cacheOracle).getInjectedWords();
        }

        @Override
        protected void createOracles() {
            for (int i = 0; i < numWorkers; i++) {
                DFA targetClone = AutomataHelper.cloneDFA(target,alphabet);
                MembershipOracle.DFAMembershipOracle<I> worker = new SimulatorOracle.DFASimulatorOracle<>(targetClone);
                StatisticOracle.DFAStatisticOracle<I> workerstat = new StatisticOracle.DFAStatisticOracle<>(worker, alphabet);
                StatisticOracle.DFAStatisticOracle<I> shadowWorkerStat = new StatisticOracle.DFAStatisticOracle<>(worker, alphabet);
                workers.add(workerstat);
                shadowWorkers.add(shadowWorkerStat);
            }
            parallelOracle = new StaticParallelOracle<>(workers, minWorkerBatchSize, ParallelOracle.PoolPolicy.FIXED);
            shadowOracle = new StaticParallelOracle<>(shadowWorkers, minWorkerBatchSize, ParallelOracle.PoolPolicy.FIXED);
            parallelStat = new StatisticOracle.DFAStatisticOracle<>(parallelOracle, alphabet);

            if (cachePolicy == CachePolicy.NormalCache)
                cacheOracle = DFACaches.createTreeCache(alphabet, parallelStat);
            else {
                cacheOracle = DFAFillCacheOracle.createTreeCacheOracle(alphabet, parallelStat, numWorkers);
            }


            List<MembershipOracle<I, Boolean>> forkList = new ArrayList<>();
            forkList.add(cacheOracle);
            forkList.add(shadowOracle);
            forkOracle = new ForkOracle<>(forkList);
            topStat = new StatisticOracle.DFAStatisticOracle<I>(forkOracle, alphabet);
        }
    }

    public static class MealyOracleChain<I, O> extends OracleChain<MealyMachine<?, I, ?, O>, I, Word<O>> {

        public MealyOracleChain(int numWorkers, CachePolicy cachePolicy, CompactMealy<I, O> target, Alphabet<I> alphabet) {
            super(numWorkers, cachePolicy, target, alphabet);
        }

        @Override
        protected void createOracles() {

            for (int i = 0; i < numWorkers; i++) {
                MealyMachine targetClone = AutomataHelper.cloneMealy(target, alphabet);
                MembershipOracle<I, Word<O>> sul = new SimulatorOracle.MealySimulatorOracle<>(targetClone);
                workers.add(new StatisticOracle.MealyStatisticOracle<>(sul, alphabet));
                shadowWorkers.add(new StatisticOracle.MealyStatisticOracle<>(sul, alphabet));
            }

            parallelOracle = new StaticParallelOracle<>(workers, minWorkerBatchSize, ParallelOracle.PoolPolicy.FIXED);
            shadowOracle = new StaticParallelOracle<>(shadowWorkers, minWorkerBatchSize, ParallelOracle.PoolPolicy.FIXED);
            parallelStat = new StatisticOracle.MealyStatisticOracle<>(parallelOracle, alphabet);

            // TODO: add MealyFillCacheOracle Here
            cacheOracle = MealyCaches.createTreeCache(alphabet, parallelStat);

            List<MembershipOracle<I, Word<O>>> forkList = new ArrayList<>();
            forkList.add(cacheOracle);
            forkList.add(shadowOracle);
            forkOracle = new ForkOracle<>(forkList);
            topStat = new StatisticOracle.MealyStatisticOracle<>(forkOracle, alphabet);
        }
    }
}
