package de.learnlib.algorithms.ttt.experiments.tools;

import de.learnlib.algorithms.ttt.experiments.statistics.QueryCounter;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;

public class StatisticWriter {
    public static void writeBatches(StatisticExperiment.StatisticExperimentDFA experiment, int experimentGroup, int numInputs, int numStates, int numOutputs, int numWorkers, int iteration, String automatType, String path, String learnerName){
        try {
            JSONObject dataJson = new JSONObject();
            dataJson.append("nIn",numInputs);
            dataJson.append("nSt",numStates);
            dataJson.append("nWo",numWorkers);
            dataJson.append("nOu",numOutputs);
            dataJson.append("it",iteration);
            dataJson.append("le",learnerName);
            dataJson.append("gr",experimentGroup);
            dataJson.append("nRo",experiment.getRoundMarkerTop().size());


            int currentRound=0;
            int currentAfterCacheIndex=0;
            QueryCounter cachedParallelSymbolCount=QueryCounter.parallelWordCount(experiment.getTopCounter(),experiment.getWorkerCounters(),experiment.getInputAlphabet());
            QueryCounter parallelSymbolCount=QueryCounter.parallelWordCount(experiment.getTopCounter(),experiment.getShadowCounters(),experiment.getInputAlphabet());
            long startTime=0,endTime=0;

            System.out.format("In:%2d St:%2d Wk:%2d It:%3d Gr:%1d\t",numInputs, numStates, numWorkers, iteration, experimentGroup);
            System.out.format("Total Batch: %d  Total Symbol:%d  Time: %d\n",experiment.getTopCounter().size(),experiment.getTopCounter().getSumWordLength(),QueryCounter.parallelWordCount(experiment.getParallelCounter(),
                    experiment.getWorkerCounters(), experiment.getInputAlphabet()).getSumWordLength());

            JSONArray batchesJson=new JSONArray();
            for(int currentBatch=0;currentBatch<experiment.getTopCounter().size();currentBatch++){
                JSONObject batchJson=new JSONObject();
                batchJson.append("b", currentBatch);
                //System.out.format("Batch: %d   RoundIndex: %d \n",currentBatch,currentRound);
                if(experiment.getRoundMarkerTop().size()>currentRound && currentBatch>=(int)(experiment.getRoundMarkerTop().get(currentRound))) currentRound++;
                batchJson.append("r",currentRound);

                QueryCounter.QueryRecord currentRecord=experiment.getTopCounter().get(currentBatch);
                batchJson.append("qT",currentRecord.getQueryCount());
                batchJson.append("sT",currentRecord.getSymbolCount(experiment.getInputAlphabet()));
                batchJson.append("scr",currentRecord.source);

                startTime=currentRecord.timestamp;
                if(experiment.getTopCounter().size()>currentBatch+1)
                    endTime=experiment.getTopCounter().get(currentBatch+1).timestamp;
                else
                    endTime=Long.MAX_VALUE;
                while(currentAfterCacheIndex<experiment.getParallelCounter().size() && experiment.getParallelCounter().get(currentAfterCacheIndex).timestamp<startTime){
                    currentAfterCacheIndex++;
                }
                if(currentAfterCacheIndex<experiment.getParallelCounter().size() && experiment.getParallelCounter().get(currentAfterCacheIndex).timestamp<endTime){//found after cache batch
                    QueryCounter.QueryRecord currentAfterCache=experiment.getParallelCounter().get(currentAfterCacheIndex);
                    batchJson.append("qAC",currentAfterCache.getQueryCount());
                    batchJson.append("sAC",currentAfterCache.getSymbolCount(experiment.getInputAlphabet()));
                }else{
                    batchJson.append("qAC",0);
                    batchJson.append("sAC",0);
                }
                batchJson.append("sPAC",cachedParallelSymbolCount.get(currentBatch).getSymbolCount(experiment.getInputAlphabet()));
                batchJson.append("sPT",parallelSymbolCount.get(currentBatch).getSymbolCount(experiment.getInputAlphabet()));
                batchesJson.put(batchJson);
            }
            dataJson.append("Batches",batchesJson);

            String file_name=String.format("exp_gr%d_in%d_st%d_wk%d_it%d.dat",experimentGroup,numInputs,numStates,numWorkers,iteration);

            PrintWriter writer = new PrintWriter(path+ File.separator+file_name, "UTF-8");

            dataJson.write(writer);
            writer.close();
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
    }
}
