package de.learnlib.algorithms.ttt.experiments;


import javafx.util.Pair;

public class QueryCallerTracer {
    public static final Callers LEARNER = Callers.Learner;
    public static final Callers EQTEST = Callers.EQTest;
    public static final Callers Others = Callers.Others;

    private static final String LEARNER_NAME = "Learner";
    private static final String EQTEST_NAME = "EQOracle";

    public static enum Callers {
        Learner,
        EQTest,
        Others
    }

    public static Pair<Callers,String> trace() {
        StackTraceElement[] stackTraceElements = Thread.currentThread().getStackTrace();
        for (StackTraceElement st : stackTraceElements) {
            if (st.getClassName().contains(LEARNER_NAME)) {
                Pair<Callers,String> result=new Pair<>(LEARNER,st.getMethodName());
                return result;
            }
            if (st.getClassName().contains(EQTEST_NAME)) {
                Pair<Callers,String> result=new Pair<>(EQTEST,st.getMethodName());
                return result;
            }
        }
        return new Pair<Callers,String>(Others, "");
    }
}
