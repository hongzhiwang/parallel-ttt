package de.learnlib.algorithms.ttt.experiments.statistics;

import de.learnlib.api.Query;
import net.automatalib.words.Alphabet;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;


public class QueryCounter extends ArrayList<QueryCounter.QueryRecord> {

    Alphabet alphabet;

    public QueryCounter(Alphabet alphabet) {
        super();
        this.alphabet = alphabet;
    }

    /**
     *
     * @param dispatcher QueryCounter from parallel Oracle
     * @param workers QueryCounters from SUL Oracle
     * @param alphabet Input alphabet
     * @return
     */
    public static QueryCounter parallelWordCount(QueryCounter dispatcher, List<QueryCounter> workers, Alphabet alphabet) {
        QueryCounter longestThread = new QueryCounter(alphabet);
//        long start = dispatcher.get(0).timestamp;
        int[] indexes = new int[workers.size()];
        for (int i = 0; i < indexes.length; i++) indexes[i] = 0;
        for (int i = 1; i <= dispatcher.size(); i++) {
            long end;
            if (i == dispatcher.size()) end = Long.MAX_VALUE;
            else end = dispatcher.get(i).timestamp;
            //System.out.printf("start: %d, end: %d \n",start,end);
            int maxWordLength = 0;
            QueryRecord longestRecord = null;
            for (int j = 0; j < workers.size(); j++) {
                int wordLength = 0;
                QueryCounter worker = workers.get(j);
                if (indexes[j] >= worker.size()) continue;
                QueryRecord record = worker.get(indexes[j]);
                while (record.timestamp < end) {
                    //System.out.printf("found: worker%d[%d]: %d:%d:%d\n",j,indexes[j],record.timestamp,record.queryCount,record.wordLength);
                    wordLength += record.getSymbolCount(alphabet);
                    indexes[j]++;
                    if (indexes[j] >= worker.size()) break;
                    record = worker.get(indexes[j]);
                }
                if (wordLength > maxWordLength) {
                    longestRecord = new QueryRecord(0, record.wordList);
                    maxWordLength = wordLength;
                }
            }
            if(longestRecord!=null)
                longestThread.add(longestRecord);
            else
                longestThread.add(new QueryRecord(0,new ArrayList<>()));
            // start = end;
        }

        return longestThread;
    }



    public int getSumWordLength() {
        int sumWordLength = 0;
        for (QueryRecord record : this)
            sumWordLength += record.getSymbolCount(alphabet);
        return sumWordLength;
    }

    public void add(Collection<? extends Query> queries) {
        add(queries, "");
    }

    public void add(Collection<? extends Query> queries, String source) {
        if (queries.isEmpty()) return;
        List<BigInteger> wordList = new ArrayList<>();
        for (Query query : queries) {
            BigInteger word = WordSerializer.wordToBigint(alphabet, query.getInput());
            wordList.add(word);
        }
        add(new QueryRecord(System.nanoTime(), wordList, source));
    }

    public double meanBatchSize() {
        if (size() == 0) return 0;
        double sumQueryCount = 0;
        for (QueryRecord batch : this)
            sumQueryCount += batch.getQueryCount();
        return sumQueryCount / size();
    }

    public int getLengthCount(QueryRecord queryRecord) {
        return queryRecord.getSymbolCount(alphabet);
    }


    public static class QueryRecord {
        public final long timestamp;
        public final List<BigInteger> wordList;
        public final String source;

        public QueryRecord(long timestamp, List<BigInteger> wordList) {
            this.timestamp = timestamp;
            this.wordList = wordList;
            source = "";
        }

        public QueryRecord(long timestamp, List<BigInteger> wordList, String source) {
            this.timestamp = timestamp;
            this.wordList = wordList;
            this.source = source;
        }

        public boolean isEarlier(QueryRecord anotherRecord) {
            return this.timestamp < anotherRecord.timestamp;
        }

        public int getQueryCount() {
            return wordList.size();
        }

        public int getSymbolCount(Alphabet alphabet){
            int symbolCount=0;
            for(BigInteger word:wordList){
                symbolCount+=WordSerializer.getLength(alphabet, word);
            }
            return symbolCount;
        }

        @Override
        public String toString(){
            StringBuilder sb=new StringBuilder();
            sb.append(timestamp).append(": ");
            sb.append(wordList.size()).append(" ").append(source);
            return sb.toString();
        }
    }
}
