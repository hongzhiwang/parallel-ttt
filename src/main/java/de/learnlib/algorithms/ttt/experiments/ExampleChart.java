/*
package de.learnlib.algorithms.ttt.Experiments;

import java.io.FileNotFoundException;
import java.util.*;

*/
/**
 * Created by yummiii on 2015/5/15.
 *//*

public class ExampleChart {
    public static void main(String[] args) throws FileNotFoundException {
        Random random = new Random();
        int maxOrder = 8, num_input = 15, num_state = 10, num_worker = 16, num_output = 8, experimentsEach = 20;

        GoogleChartGenerator gChart = new GoogleChartGenerator("example.html", "Examples");
        Map<String, Integer> para = new LinkedHashMap<>();

        String title = "Size of Batches (DFA)";
        Collection[] results = ExperimentRunnerOld.runDFA(num_input, num_state, num_worker, null, false, random, false, true);
        para.put("Inputs", num_input);
        para.put("States", num_state);
        para.put("Workers", num_worker);

        gChart.addBarChart((List<List<Integer>>) results[0], results[1], results[2], title, para);

        num_input = 10;
        num_output = 5;
        num_state = 10;
        title = "Size of Batches (Mealy)";
        results = ExperimentRunnerOld.runMealy(num_input, num_output, num_state, num_worker, random, false, false);
        para = new LinkedHashMap<>();
        para.put("Inputs", num_input);
        para.put("Outputs", num_output);
        para.put("States", num_state);

        gChart.addBarChart((List<List<Integer>>) results[0], results[1], results[2], title, para);

        num_input = 4;
        num_output = 8;
        title = "Mean Size of Batches (Different Number of States)";
        boolean countCache = true;
        Map<Integer, Double> meanSize = null;
        meanSize = ExperimentRunnerOld.runMeanBatchState(maxOrder, num_input, num_worker, num_output, random, experimentsEach, countCache);
        para = new LinkedHashMap<>();
        para.put("Inputs", num_input);
        para.put("Outputs", num_output);
        para.put("Workers", num_worker);

        gChart.addLineChart(meanSize, title, "Number of States", para, true);

        num_state = 10;
        num_output = 4;
        maxOrder = 7;
        title = "Mean Size of Batches (Different Number of Inputs)";
        para = new LinkedHashMap<>();
        para.put("States", num_state);
        para.put("Outputs", num_output);
        para.put("Workers", num_worker);
        meanSize = ExperimentRunnerOld.runMeanBatchInput(maxOrder, num_state, num_worker, num_output, random, experimentsEach, countCache);

        gChart.addLineChart(meanSize, title, "Number of Inputs", para, true);

        gChart.write();


    }
}
*/
