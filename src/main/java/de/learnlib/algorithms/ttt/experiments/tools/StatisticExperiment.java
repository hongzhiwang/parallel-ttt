package de.learnlib.algorithms.ttt.experiments.tools;

import de.learnlib.algorithms.ttt.experiments.statistics.QueryCounter;
import de.learnlib.algorithms.ttt.experiments.statistics.WordSerializer;
import de.learnlib.api.EquivalenceOracle;
import de.learnlib.api.LearningAlgorithm;
import de.learnlib.api.MembershipOracle;
import de.learnlib.oracles.DefaultQuery;
import net.automatalib.automata.Automaton;
import net.automatalib.automata.fsa.DFA;
import net.automatalib.automata.fsa.impl.compact.CompactDFA;
import net.automatalib.words.Alphabet;

import java.math.BigInteger;
import java.util.*;

public abstract class StatisticExperiment<A extends Automaton, I, D> {
    protected LearningAlgorithm<A, I, D> learner;
    protected EquivalenceOracle<A, I, D> eqOracle;
    protected OracleChain<A, I, D> oracleChain;

    // Statistics
    protected List<Integer> roundMarkerTop = new ArrayList<>();
    protected List<Integer> roundMarkerParallel = new ArrayList<>();
    protected List<BigInteger> counterExamples = new ArrayList<>();

    // Result Automata
    private A learnResult;

    private boolean runFlag = false;

    public void run() {

        if (runFlag) return;
        runFlag = true;
        assert learner!=null && eqOracle!=null;

        try {
            learner.startLearning();
            boolean done = false;
            A hyp = null;
            while (!done) {
                roundMarkerTop.add(getTopCounter().size());
                roundMarkerParallel.add(getParallelCounter().size());
                hyp = learner.getHypothesisModel();

                DefaultQuery<I, D> ce = eqOracle.findCounterExample(hyp, oracleChain.getInputAlphabet());
                if (ce == null) {
                    done = true;
                    continue;
                }
                counterExamples.add(WordSerializer.wordToBigint(oracleChain.getInputAlphabet(),ce.getInput()));
                learner.refineHypothesis(ce);

            }
            learnResult = hyp;

        } finally {

            oracleChain.shutdown();
        }
    }

    public QueryCounter getTopCounter() {
        return oracleChain.getTopCounter();
    }

    public QueryCounter getParallelCounter() {
        return oracleChain.getParallelCounter();
    }

    public List<QueryCounter> getWorkerCounters() {
        return oracleChain.getWorkerCounters();
    }

    public List<QueryCounter> getShadowCounters() {
        return oracleChain.getShadowCounters();
    }

    public List<Integer> getRoundMarkerTop() {
        return roundMarkerTop;
    }

    public List<Integer> getRoundMarkerParallel() {
        return roundMarkerParallel;
    }

    public MembershipOracle<I,D> getTopOracle(){return oracleChain.getTopOracle();}

    public A getTarget() {
        return oracleChain.getTarget();
    }

    public Alphabet<I> getInputAlphabet(){return oracleChain.getInputAlphabet();}

    public A getLearnResult() {
        return learnResult;
    }

    public List<BigInteger> getCounterExamples(){ return counterExamples;}

    public void setLearner(LearningAlgorithm<A,I,D> learner){
        this.learner=learner;
    }
    public void setEqOracle(EquivalenceOracle<A,I,D> eqOracle){
        this.eqOracle=eqOracle;
    }


    public static class StatisticExperimentDFA<I> extends StatisticExperiment<DFA<?,I>, I, Boolean> {
        public StatisticExperimentDFA(int numStates, int numWorkers, Alphabet<I> alphabet, OracleChain.CachePolicy cachePolicy, Random random) {
            DFA<?,I> target = AutomataHelper.randomDFA(alphabet, numStates, random);
            oracleChain = new OracleChain.DFAOracleChain<>(numWorkers, cachePolicy, target, alphabet);
        }

        public StatisticExperimentDFA(CompactDFA<I> target, Alphabet<I> alphabet, OracleChain.CachePolicy cachePolicy, int numWorkers) {
            oracleChain=new OracleChain.DFAOracleChain<>(numWorkers, cachePolicy, target, alphabet);
        }
    }
}
