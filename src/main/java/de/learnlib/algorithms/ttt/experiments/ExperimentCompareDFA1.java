package de.learnlib.algorithms.ttt.experiments;

import de.learnlib.algorithms.ttt.dfa.TTTParallelLearnerDFA;
import de.learnlib.algorithms.ttt.dfa.TTTParallelLearnerDFABuilder;
import de.learnlib.algorithms.ttt.experiments.statistics.QueryCounter;
import de.learnlib.algorithms.ttt.experiments.tools.ExperimentComparator;
import de.learnlib.algorithms.ttt.experiments.tools.OracleChain;
import de.learnlib.algorithms.ttt.experiments.tools.StatisticExperiment;
import de.learnlib.algorithms.ttt.experiments.tools.StatisticWriter;
import de.learnlib.api.MembershipOracle;
import de.learnlib.parallelism.ParallelIncrementalWMethodEQOracle;
import net.automatalib.words.Alphabet;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.*;

public class ExperimentCompareDFA1 {
    public static void main(String[] args) {
        Comparator comparator = new Comparator(5, 10, 4, 5, 2, 2, 4, 8, 10, new Random(), OracleChain.CachePolicy.FillCache, OracleChain.CachePolicy.NormalCache);
        comparator.run();
    }

    static class Comparator extends ExperimentComparator.ExperimentComparatorDFA<
            TTTParallelLearnerDFA<Integer>,
            TTTParallelLearnerDFA<Integer>,
            ParallelIncrementalWMethodEQOracle.DFAParallelIncrementalWMethodEQOracle<Integer>,
            ParallelIncrementalWMethodEQOracle.DFAParallelIncrementalWMethodEQOracle<Integer>
            > {

        private final String path;
        private final String[] learnerNames = {"TTT Fill", "TTT Normal"};
        private final int totalRun;
        private int runCounter = 0;
        private List<Map<String, Integer>> summarizeExps = new ArrayList<>();

        /**
         * Defines range of these parameters. the comparator will iterate through every combination.
         * Note: end number must greater or equal to the start number, and the step of iterations is always 1.
         */
        protected Comparator(int numStatesStart, int numStatesEnd, int numInputsStart, int numInputsEnd, int numOutputsStart, int numOutputsEnd, int numWorkersStart, int numWorkersEnd, int numIterations, Random random, OracleChain.CachePolicy cachePolicy1, OracleChain.CachePolicy cachePolicy2) {
            super(numStatesStart, numStatesEnd, numInputsStart, numInputsEnd, numOutputsStart, numOutputsEnd, numWorkersStart, numWorkersEnd, numIterations, random, cachePolicy1, cachePolicy2);
            path = "/home/yummiii/projects/parallelttt/results/exp_" + new SimpleDateFormat("yyyyMMddHHmmss").format(new Date());
            File dir = new File(path);
            if (!dir.exists())
                dir.mkdirs();
            totalRun = (numInputsEnd - numInputsStart + 1) * (numOutputsEnd - numOutputsStart + 1) * (numStatesEnd -
                    numStatesStart + 1) * (numWorkersEnd - numInputsStart + 1) * numIterations * 2;
        }

        @Override
        protected TTTParallelLearnerDFA<Integer> defineLearner1(Alphabet alphabet, MembershipOracle oracle) {
            return new TTTParallelLearnerDFABuilder<Integer>().withAlphabet(alphabet).withOracle(oracle).create();
        }

        @Override
        protected TTTParallelLearnerDFA<Integer> defineLearner2(Alphabet alphabet, MembershipOracle oracle) {
            return new TTTParallelLearnerDFABuilder<Integer>().withAlphabet(alphabet).withOracle(oracle).create();
        }

        @Override
        protected ParallelIncrementalWMethodEQOracle.DFAParallelIncrementalWMethodEQOracle<Integer> defineEquivalenceOracle1(Alphabet alphabet, MembershipOracle oracle, int numWorkers) {
            return new ParallelIncrementalWMethodEQOracle.DFAParallelIncrementalWMethodEQOracle<>(alphabet, oracle, 2, 2 * numWorkers);
        }

        @Override
        protected ParallelIncrementalWMethodEQOracle.DFAParallelIncrementalWMethodEQOracle<Integer> defineEquivalenceOracle2(Alphabet alphabet, MembershipOracle oracle, int numWorkers) {
            return new ParallelIncrementalWMethodEQOracle.DFAParallelIncrementalWMethodEQOracle<>(alphabet, oracle, 2, 2 * numWorkers);
        }

        @Override
        protected void saveExperimentResults(StatisticExperiment.StatisticExperimentDFA experiment, int experimentGroup, int numInputs, int numStates, int numOutputs, int numWorkers, int iteration, String automatType) {
            System.out.printf("[%6d/%6d] ", ++runCounter, totalRun);
            StatisticWriter.writeBatches(experiment, experimentGroup, numInputs, numStates, numOutputs, numWorkers, iteration, automatType, path, learnerNames[experimentGroup - 1]);
            Map<String, Integer> currExp = new HashMap<>();
            currExp.put("nIn", numInputs);
            currExp.put("nSt", numStates);
            currExp.put("nWo", numWorkers);
            currExp.put("nOu", numOutputs);
            currExp.put("it", iteration);
            currExp.put("gr", experimentGroup);
            currExp.put("qT", experiment.getTopCounter().size());
            currExp.put("sT", experiment.getTopCounter().getSumWordLength());
            currExp.put("qAC", experiment.getParallelCounter().size());
            currExp.put("sAC", experiment.getParallelCounter().getSumWordLength());
            currExp.put("sPT", QueryCounter.parallelWordCount(experiment.getTopCounter(),
                    experiment.getShadowCounters(), experiment.getInputAlphabet()).getSumWordLength());
            currExp.put("sPAC", QueryCounter.parallelWordCount(experiment.getParallelCounter(),
                    experiment.getWorkerCounters(), experiment.getInputAlphabet()).getSumWordLength());
            currExp.put("nRo", experiment.getRoundMarkerTop().size());
            summarizeExps.add(currExp);

        }

        @Override
        protected void afterEveryExperiment(StatisticExperiment.StatisticExperimentDFA experiment1, StatisticExperiment.StatisticExperimentDFA experiment2) {
        }

        @Override
        protected void afterRun() {
            try {
                PrintWriter writer = new PrintWriter(path + File.separator + "summarize");
                writer.format("%8s,%8s,%8s,%8s,%8s,%8s,%8s,%8s,%8s,%8s,%8s,%8s,%8s,%8s\n", "nIn", "nSt", "nWo", "nOu",
                        "it", "le", "gr", "nRo", "qT", "sT", "qAC", "sAC", "sPT", "sPAC");
                for (Map<String, Integer> exp : summarizeExps) {
                    writer.format("%8d,%8d,%8d,%8d,%8d,%8s,%8d,%8d,%8d,%8d,%8d,%8d,%8d,%8d\n", exp.get("nIn"),
                            exp.get("nSt"), exp.get("nWo"), exp.get("nOu"), exp.get("it"),
                            learnerNames[exp.get("gr") - 1], exp.get("gr"), exp.get("nRo"), exp.get("qT"),
                            exp.get("sT"), exp.get("qAC"), exp.get("sAC"), exp.get("sPT"), exp.get("sPAC"));
                }
                writer.close();
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        }
    }
}
