package de.learnlib.algorithms.ttt.experiments;

public class Proposal {
    /*
    static final int numWorker = 8;

    static CompactDFA<Character> exampleDFA() {
        Alphabet<Character> sigma = Alphabets.characters('a', 'b');
        CompactDFA<Character> dfa = new CompactDFA<>(sigma);
        int q0 = dfa.addInitialState(false);
        int q1 = dfa.addState(false);
        int q2 = dfa.addState(false);
        int q3 = dfa.addState(true);
        dfa.addTransition(q0, 'a', q1);
        dfa.addTransition(q0, 'b', q0);
        dfa.addTransition(q1, 'a', q2);
        dfa.addTransition(q1, 'b', q1);
        dfa.addTransition(q2, 'a', q3);
        dfa.addTransition(q2, 'b', q2);
        dfa.addTransition(q3, 'a', q0);
        dfa.addTransition(q3, 'b', q3);
        return dfa;
    }

    static CompactDFA<Character> DFA_1() {
        Alphabet<Character> sigma = Alphabets.characters('a', 'b');
        CompactDFA<Character> dfa = new CompactDFA<Character>(sigma);
        int q0 = dfa.addInitialState(true);
        int q1 = dfa.addState(false);
        int q2 = dfa.addState(true);
        int q3 = dfa.addState(false);
        int q4 = dfa.addState(false);
        int q5 = dfa.addState(false);
        dfa.addTransition(q0, 'a', q3);
        dfa.addTransition(q0, 'b', q1);
        dfa.addTransition(q1, 'a', q1);
        dfa.addTransition(q1, 'b', q2);
        dfa.addTransition(q2, 'a', q0);
        dfa.addTransition(q2, 'b', q3);
        dfa.addTransition(q3, 'a', q4);
        dfa.addTransition(q3, 'b', q0);
        dfa.addTransition(q4, 'a', q4);
        dfa.addTransition(q4, 'b', q5);
        dfa.addTransition(q5, 'a', q5);
        dfa.addTransition(q5, 'b', q3);
        return dfa;
    }

    public static void main(String[] args) throws IOException {
        //CompactDFA<Integer> dfa=ExperimentRunner.randomDFA(2,6,new Random());
//        CompactDFA<Character> dfa=DFA_1();
//        Writer w = DOT.createDotWriter(true);
//        GraphDOT.write(dfa, dfa.getInputAlphabet(), w);
//        w.close();
        runDFA();
//        runMealy();
    }

    private static void writeToFile(Collection<Map.Entry<String, String>> graphics,String prefix) throws IOException, InterruptedException {
        String path="/home/yummiii/Documents/output/";
        for(File file: new File(path).listFiles())
            if(file.getName().startsWith(prefix))
                file.delete();

        int index=0;
        for(Map.Entry<String,String> entry:graphics){
            String name=entry.getKey();
            String file=String.format("%s%s_%03d_%s.dot",path,prefix,index++,name);
            PrintWriter fileWriter=new PrintWriter(file,"UTF-8");
            fileWriter.write(entry.getValue());
            fileWriter.close();
            System.out.println(file);
            Process process=Runtime.getRuntime().exec("dot "+file+" -Tpng -o "+file.replace(".dot",".png"));
            process.waitFor();
        }
    }
    public static CompactDFA<Character> randomDFA(int numInput, int numState, Random random) {
        while (true) {
            Alphabet<Character> sigma = Alphabets.characters((char) 97,(char)(97 + numInput - 1));

            CompactDFA<Character> dfa = RandomAutomata.randomDFA(random, numState, sigma, true);
            if(dfa.getStates().size()==numState)
                return dfa;
        }
    }
    private static void runDFA() {
        CompactDFA<Character> targetOriginal = randomDFA(2, 6, new Random());
        targetOriginal=DFA_1();


        List<StatisticOracle.DFAStatisticOracle<Character>> oracles = new ArrayList<>();
        List<StatisticOracle.DFAStatisticOracle<Character>> shadowOracles = new ArrayList<>();

        for (int i = 0; i < numWorker; i++) {
            CompactDFA<Character> target = cloneDFA(targetOriginal);
            MembershipOracle.DFAMembershipOracle<Character> sul = new SimulatorOracle.DFASimulatorOracle<>(target);
            oracles.add(new StatisticOracle.DFAStatisticOracle<>(sul, targetOriginal.getInputAlphabet()));
            shadowOracles.add(new StatisticOracle.DFAStatisticOracle<>(sul, targetOriginal.getInputAlphabet()));
        }

        ParallelOracle<Character, Boolean> parallelOracle = new StaticParallelOracle<>(oracles, 1, ParallelOracle.PoolPolicy.FIXED);
        ParallelOracle<Character, Boolean> shadowParallelOracle = new StaticParallelOracle<>(shadowOracles, 1, ParallelOracle.PoolPolicy.FIXED);
        StatisticOracle.DFAStatisticOracle<Character> statisticParallelOracle = new StatisticOracle.DFAStatisticOracle<>(parallelOracle, targetOriginal.getInputAlphabet());

        DFACacheOracle<Character> cacheOracle = DFACaches.createTreeCache(targetOriginal.getInputAlphabet(), statisticParallelOracle);

        StatisticOracle.DFAStatisticOracle<Character> statisticCacheOracle
                = new StatisticOracle.DFAStatisticOracle<>(cacheOracle, targetOriginal.getInputAlphabet());

        List<MembershipOracle<Character, Boolean>> forkList = new ArrayList<>();
        forkList.add(shadowParallelOracle);
        forkList.add(statisticCacheOracle);
        ForkOracle<Character, Boolean> forkOracle = new ForkOracle<>(forkList);

        TTTParallelLearnerDFA learner = new TTTParallelLearnerDFABuilder<Character>()
                .withAlphabet(targetOriginal.getInputAlphabet())
                .withOracle(forkOracle)
                .create();

        ParallelIncrementalWMethodEQOracle.DFAParallelIncrementalWMethodEQOracle<Character> equivalenceOracle;
        equivalenceOracle = new ParallelIncrementalWMethodEQOracle.DFAParallelIncrementalWMethodEQOracle<>
                (targetOriginal.getInputAlphabet(), forkOracle, 3, numWorker);

        List<Integer> roundMarkerCache = new ArrayList<>();
        List<Integer> roundMarkerParallel = new ArrayList<>();

        try {
            StringBuffer sb=new StringBuffer();
            GraphDOT.write(targetOriginal,targetOriginal.getInputAlphabet(),sb);
            learner.graphics.add(new AbstractMap.SimpleEntry<>("target",sb.toString()));
            learner.startLearning();
            boolean done = false;
            DFA hyp;
            while (!done) {
                roundMarkerCache.add(statisticCacheOracle.getCounter().size());
                roundMarkerParallel.add(statisticParallelOracle.getCounter().size());
                hyp = learner.getHypothesisModel();

                DefaultQuery<Character, Boolean> ce = equivalenceOracle.findCounterExample(hyp, targetOriginal.getInputAlphabet());
                if (ce == null) {
                    done = true;
                    continue;
                }

                learner.refineHypothesis(ce);
            }
            writeToFile(learner.graphics,"g");
            writeToFile(learner.dtGraphics,"dt");
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            parallelOracle.shutdown();
            shadowParallelOracle.shutdown();
            QueryCounter counter= statisticCacheOracle.getCounter();
            for(QueryCounter.QueryRecord record:counter){
                System.out.printf("Source: %s   Queries: %d   Words:%d\n",record.source,record.getQueryCount(),counter.getLengthCount(record));
            }
        }
    }

    public static String drawTTTDot(BaseTTTParallelLearner learner) {
        StringBuffer dot = new StringBuffer();

        DiscriminationTree discriminationTree = learner.getDiscriminationTree();
        GraphDOTHelper dtHelper = discriminationTree.graphView().getGraphDOTHelper();

        Stack<DTNode> nodestack = new Stack<>();
        nodestack.push(discriminationTree.getRoot());

        dot.append("digraph Proposal{\n");
//        dot.append("subgraph cluster_dt{\n");

        Map<String, String> prop = new HashMap<>();


        while (!nodestack.isEmpty()) {
            DTNode curr = nodestack.pop();
            prop.clear();
            dtHelper.getNodeProperties(curr, prop);
            int currId = System.identityHashCode(curr);
            dot.append("\t").append(currId);
            addParam(prop, dot);
            dot.append(";\n");
            if (!curr.isLeaf()) {
                for (Object child : curr.getChildren())
                    nodestack.push((DTNode) child);
            }
            if (curr != discriminationTree.getRoot()) {
                DTNode parent = curr.getParent();
                int parentId = System.identityHashCode(parent);
                prop.clear();
                dtHelper.getEdgeProperties(parent, curr, curr, prop);
                prop.put("splines", "line");
                dot.append("\t").append(parentId).append("->").append(currId);
                addParam(prop, dot);
                dot.append(";\n");
            }
        }

//        dot.append("}\nsubgraph cluster_automat{\n");


        TTTHypothesis hyp = learner.getHypothesisDS();

        Alphabet sigma = hyp.getInputAlphabet();
        GraphDOTHelper hypHelper = learner.getHypothesisDOTHelper();
        for (TTTState state : (Collection<TTTState>) hyp.getStates()) {
            int currId = System.identityHashCode(state);
            prop.clear();
//            prop.put("label", String.valueOf(state));
//            prop.put("shape", "circle");
//            hypHelper.getNodeProperties(state, prop);
//            dot.append("\t").append(currId);
//            addParam(prop, dot);
//            dot.append(";\n");
//            Collection<TTTHypothesis.TTTEdge> outTrans=hyp.graphView().getOutgoingEdges(state);
//            for(TTTHypothesis.TTTEdge edge:outTrans) {
//                prop.clear();
//                TTTState target = edge.target;
//                int targetId = System.identityHashCode(target);
//                prop.put("label",String.valueOf(edge.transition.getInput()));
//                hypHelper.getEdgeProperties(state, edge, target, prop);
//
//                dot.append("\t").append(currId).append("->").append(targetId);
//                addParam(prop, dot);
//                dot.append(";\n");
//            }
            int nodeId = System.identityHashCode(state.getDTLeaf());
            for (Object sym : sigma) {
                TTTTransition transition = hyp.getInternalTransition(state, sym);
                DTNode target = transition.getDTTarget();
                int targetId = System.identityHashCode(target);
                prop.clear();
                String label = String.valueOf(sym);
                if (transition instanceof TTTTransitionMealy)
                    label += "/" + String.valueOf(((TTTTransitionMealy) transition).getOutput());
                prop.put("label", label);
                prop.put("style", "dotted");
                dot.append("\t").append(nodeId).append("->").append(targetId);
                addParam(prop, dot);
                dot.append(";\n");
            }
        }


        dot.append("}\n");
        return dot.toString();
    }

    public static String drawDT(BaseTTTParallelLearner learner){
        StringBuffer dot=new StringBuffer();
        try {
            GraphDOT.write(learner.getDiscriminationTree().graphView(),dot);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return dot.toString();
    }

    private static void addParam(Map<String, String> prop, Appendable a) {
        try {
            a.append("[");
            List<String> propList = new ArrayList<>();
            for (String key : prop.keySet()) {
                propList.add(String.format("%s=\"%s\"", key, prop.get(key)));
            }
            a.append(propList.stream().collect(Collectors.joining(", ")));
            a.append("]");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void runMealy() {
        CompactMealy<Integer, Character> targetOriginal = randomMealy(5, 3, 10, new Random());
        List<StatisticOracle.MealyStatisticOracle<Integer, Character>> oracles = new ArrayList<>();
        List<StatisticOracle.MealyStatisticOracle<Integer, Character>> shadowOracles = new ArrayList<>();


        for (int i = 0; i < 8; i++) {
            CompactMealy<Integer, Character> target = cloneMealy(targetOriginal);
            MembershipOracle.MealyMembershipOracle<Integer, Character> sul = new SimulatorOracle.MealySimulatorOracle<>(target);
            oracles.add(new StatisticOracle.MealyStatisticOracle<>(sul, targetOriginal.getInputAlphabet()));
            shadowOracles.add(new StatisticOracle.MealyStatisticOracle<>(sul, targetOriginal.getInputAlphabet()));
        }

        ParallelOracle<Integer, Word<Character>> parallelOracle = new StaticParallelOracle<>(oracles, 1, ParallelOracle.PoolPolicy.FIXED);
        ParallelOracle<Integer, Word<Character>> shadowParallelOracle = new StaticParallelOracle<>(shadowOracles, 1, ParallelOracle.PoolPolicy.FIXED);
        StatisticOracle.MealyStatisticOracle<Integer, Character> statisticParallelOracle = new StatisticOracle.MealyStatisticOracle<>(parallelOracle, targetOriginal.getInputAlphabet());

        MealyCacheOracle<Integer, Character> cacheOracle = MealyCaches.createTreeCache(targetOriginal.getInputAlphabet(), statisticParallelOracle);

        StatisticOracle.MealyStatisticOracle<Integer, Character> statisticCacheOracle
                = new StatisticOracle.MealyStatisticOracle<>(cacheOracle, targetOriginal.getInputAlphabet());

        List<MembershipOracle<Integer, Word<Character>>> forkList = new ArrayList<>();
        forkList.add(shadowParallelOracle);
        forkList.add(statisticCacheOracle);
        ForkOracle<Integer, Word<Character>> forkOracle = new ForkOracle<>(forkList);

        TTTParallelLearnerMealy learner = new TTTParallelLearnerMealyBuilder<Integer, Character>()
                .withAlphabet(targetOriginal.getInputAlphabet())
                .withOracle(forkOracle)
                .create();

        ParallelIncrementalWMethodEQOracle.MealyParallelIncrementalWMethodEQOracle<Integer, Character> equivalenceOracle;
        equivalenceOracle = new ParallelIncrementalWMethodEQOracle.MealyParallelIncrementalWMethodEQOracle<>
                (targetOriginal.getInputAlphabet(), forkOracle, 1, 8 * 2);

        List<Integer> roundMarkerCache = new ArrayList<>();
        List<Integer> roundMarkerParallel = new ArrayList<>();

        try {
            StringBuffer sb=new StringBuffer();
            GraphDOT.write(targetOriginal,targetOriginal.getInputAlphabet(),sb);
            learner.graphics.add(new AbstractMap.SimpleEntry<>("target",sb.toString()));
            learner.startLearning();
            boolean done = false;
            MealyMachine hyp;
            while (!done) {
                roundMarkerCache.add(statisticCacheOracle.getCounter().size());
                roundMarkerParallel.add(statisticParallelOracle.getCounter().size());
                hyp = learner.getHypothesisModel();

                DefaultQuery<Integer, Word<Character>> ce = equivalenceOracle.findCounterExample(hyp, targetOriginal.getInputAlphabet());
                if (ce == null) {
                    done = true;
                    continue;
                }

                learner.refineHypothesis(ce);
            }
            writeToFile(learner.graphics,"g");
            writeToFile(learner.dtGraphics,"dt");
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            parallelOracle.shutdown();
            shadowParallelOracle.shutdown();
        }
    }
    */
}
