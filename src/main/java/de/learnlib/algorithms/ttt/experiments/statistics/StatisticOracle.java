/* Copyright (C) 2013-2014 TU Dortmund
 * This file is part of LearnLib, http://www.learnlib.de/.
 * 
 * LearnLib is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License version 3.0 as published by the Free Software Foundation.
 * 
 * LearnLib is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with LearnLib; if not, see
 * <http://www.gnu.de/documents/lgpl.en.html>.
 */

package de.learnlib.algorithms.ttt.experiments.statistics;

import de.learnlib.algorithms.ttt.experiments.QueryCallerTracer;
import de.learnlib.api.MembershipOracle;
import de.learnlib.api.Query;
import javafx.util.Pair;
import net.automatalib.words.Alphabet;
import net.automatalib.words.Word;

import javax.annotation.ParametersAreNonnullByDefault;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;


@ParametersAreNonnullByDefault
public class StatisticOracle<I, D> implements MembershipOracle<I, D> {

    public static class DFAStatisticOracle<I> extends StatisticOracle<I, Boolean>
            implements MembershipOracle.DFAMembershipOracle<I> {
        public DFAStatisticOracle(MembershipOracle<I, Boolean> nextOracle, Alphabet<I> alphabet) {
            super(nextOracle, alphabet);
        }
    }

    public static class MealyStatisticOracle<I, O> extends StatisticOracle<I, Word<O>>
            implements MembershipOracle.MealyMembershipOracle<I, O> {
        public MealyStatisticOracle(MembershipOracle<I, Word<O>> nextOracle, Alphabet<I> alphabet) {
            super(nextOracle, alphabet);
        }
    }

    private final QueryCounter counter;

    private MembershipOracle<I, D> nextOracle;

    public StatisticOracle(MembershipOracle<I, D> nextOracle, Alphabet alphabet) {
        this.nextOracle = nextOracle;
        counter = new QueryCounter(alphabet);
    }

    @Override
    public void processQueries(Collection<? extends Query<I, D>> queries) {

        long timestamp = System.nanoTime();
        List<Integer> wordLengths = new ArrayList<>();
        for (Query<I, D> query : queries) {
            wordLengths.add(query.getInput().length());
        }
        Pair<QueryCallerTracer.Callers, String> result = QueryCallerTracer.trace();
        if (result != null) {
            String source;
            if(result.getKey()==QueryCallerTracer.LEARNER) source=result.getValue();
            else if(result.getKey()==QueryCallerTracer.EQTEST) source="EquivalenceTest";
            else source="Others";
            counter.add(queries,source);
        }
        nextOracle.processQueries(queries);
    }

    public QueryCounter getCounter(){return counter;}

    public void clearCounter(){
        counter.clear();
    }

}
