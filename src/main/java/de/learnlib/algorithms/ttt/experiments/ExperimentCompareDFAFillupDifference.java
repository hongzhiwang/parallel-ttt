package de.learnlib.algorithms.ttt.experiments;

import de.learnlib.algorithms.ttt.dfa.TTTParallelLearnerDFA;
import de.learnlib.algorithms.ttt.dfa.TTTParallelLearnerDFABuilder;
import de.learnlib.algorithms.ttt.experiments.statistics.WordSerializer;
import de.learnlib.algorithms.ttt.experiments.tools.ExperimentComparator;
import de.learnlib.algorithms.ttt.experiments.tools.OracleChain;
import de.learnlib.algorithms.ttt.experiments.tools.StatisticExperiment;
import de.learnlib.api.MembershipOracle;
import de.learnlib.parallelism.ParallelIncrementalWMethodEQOracle;
import net.automatalib.util.graphs.dot.GraphDOT;
import net.automatalib.words.Alphabet;

import java.io.*;
import java.math.BigInteger;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Random;

public class ExperimentCompareDFAFillupDifference {
    public static void main(String[] args) {
        Comparator comparator = new Comparator(5, 5, 4, 4, 2, 2, 4, 4, 10, new Random(), OracleChain.CachePolicy.NormalCache, OracleChain.CachePolicy.NormalCache);
        comparator.run();
    }

    static class Comparator extends ExperimentComparator.ExperimentComparatorDFA<
            TTTParallelLearnerDFA<Integer>,
            TTTParallelLearnerDFA<Integer>,
            ParallelIncrementalWMethodEQOracle.DFAParallelIncrementalWMethodEQOracle<Integer>,
            ParallelIncrementalWMethodEQOracle.DFAParallelIncrementalWMethodEQOracle<Integer>
            > {

        private final String path;

        /**
         * Defines range of these parameters. the comparator will iterate through every combination.
         * Note: end number must greater or equal to the start number, and the step of iterations is always 1.
         *

         */
        protected Comparator(int numStatesStart, int numStatesEnd, int numInputsStart, int numInputsEnd, int numOutputsStart, int numOutputsEnd, int numWorkersStart, int numWorkersEnd, int numIterations, Random random, OracleChain.CachePolicy cachePolicy1, OracleChain.CachePolicy cachePolicy2) {
            super(numStatesStart, numStatesEnd, numInputsStart, numInputsEnd, numOutputsStart, numOutputsEnd, numWorkersStart, numWorkersEnd, numIterations, random, cachePolicy1, cachePolicy2);
            path="/home/yummiii/projects/parallelttt/results/exp_"+new SimpleDateFormat("yyyyMMddHHmmss").format(new Date());
            File dir=new File(path);
            if(!dir.exists())
                dir.mkdirs();

        }

        @Override
        protected TTTParallelLearnerDFA<Integer> defineLearner1(Alphabet alphabet, MembershipOracle oracle) {
            return new TTTParallelLearnerDFABuilder<Integer>().withAlphabet(alphabet).withOracle(oracle).create();
        }

        @Override
        protected TTTParallelLearnerDFA<Integer> defineLearner2(Alphabet alphabet, MembershipOracle oracle) {
            return new TTTParallelLearnerDFABuilder<Integer>().withAlphabet(alphabet).withOracle(oracle).create();
        }

        @Override
        protected ParallelIncrementalWMethodEQOracle.DFAParallelIncrementalWMethodEQOracle<Integer> defineEquivalenceOracle1(Alphabet alphabet, MembershipOracle oracle, int numWorkers) {
            return new ParallelIncrementalWMethodEQOracle.DFAParallelIncrementalWMethodEQOracle<>(alphabet, oracle, 2, 2 * numWorkers);
        }

        @Override
        protected ParallelIncrementalWMethodEQOracle.DFAParallelIncrementalWMethodEQOracle<Integer> defineEquivalenceOracle2(Alphabet alphabet, MembershipOracle oracle, int numWorkers) {
            return new ParallelIncrementalWMethodEQOracle.DFAParallelIncrementalWMethodEQOracle<>(alphabet, oracle, 2, 2 * numWorkers);
        }

        @Override
        protected void saveExperimentResults(StatisticExperiment.StatisticExperimentDFA experiment, int experimentGroup, int numInputs, int numStates, int numOutputs, int numWorkers, int iteration, String automatType) {
/*            try {
                JSONObject dataJson = new JSONObject();
                dataJson.append("numInputs",numInputs);
                dataJson.append("numStates",numStates);
                dataJson.append("numWorkers",numWorkers);
                dataJson.append("numOutputs",numOutputs);
                dataJson.append("iteration",iteration);
                dataJson.append("learner","TTTParallelDFA "+(experimentGroup==1?cachePolicy1:cachePolicy2));
                dataJson.append("experimentGroup",experimentGroup);
                dataJson.append("rounds",experiment.getRoundMarkerTop().size());


                int currentRound=0;
                int currentAfterCacheIndex=0;
                QueryCounter cachedParallelSymbolCount=QueryCounter.parallelWordCount(experiment.getTopCounter(),experiment.getWorkerCounters(),experiment.getInputAlphabet());
                QueryCounter parallelSymbolCount=QueryCounter.parallelWordCount(experiment.getTopCounter(),experiment.getShadowCounters(),experiment.getInputAlphabet());
                long startTime=0,endTime=0;

                System.out.format("In:%d St:%d Wk:%d It:%d Gr:%d\t",numInputs, numStates, numWorkers, iteration, experimentGroup);
                System.out.format("Total Batch: %d\n",experiment.getTopCounter().size());

                JSONArray batchesJson=new JSONArray();
                for(int currentBatch=0;currentBatch<experiment.getTopCounter().size();currentBatch++){
                    JSONObject batchJson=new JSONObject();
                    batchJson.append("Batch", currentBatch);
                    //System.out.format("Batch: %d   RoundIndex: %d \n",currentBatch,currentRound);
                    if(experiment.getRoundMarkerTop().size()>currentRound && currentBatch>=(int)(experiment.getRoundMarkerTop().get(currentRound))) currentRound++;
                    batchJson.append("Round",currentRound);

                    QueryCounter.QueryRecord currentRecord=experiment.getTopCounter().get(currentBatch);
                    batchJson.append("QueryCountTop",currentRecord.getQueryCount());
                    batchJson.append("SymbolCountTop",currentRecord.getSymbolCount(experiment.getInputAlphabet()));
                    batchJson.append("Source",currentRecord.source);

                    startTime=currentRecord.timestamp;
                    if(experiment.getTopCounter().size()>currentBatch+1)
                        endTime=experiment.getTopCounter().get(currentBatch+1).timestamp;
                    else
                        endTime=Long.MAX_VALUE;
                    while(currentAfterCacheIndex<experiment.getParallelCounter().size() && experiment.getParallelCounter().get(currentAfterCacheIndex).timestamp<startTime){
                        currentAfterCacheIndex++;
                    }
                    if(currentAfterCacheIndex<experiment.getParallelCounter().size() && experiment.getParallelCounter().get(currentAfterCacheIndex).timestamp<endTime){//found after cache batch
                        QueryCounter.QueryRecord currentAfterCache=experiment.getParallelCounter().get(currentAfterCacheIndex);
                        batchJson.append("QueryCountAfterCache",currentAfterCache.getQueryCount());
                        batchJson.append("SymbolCountAfterCache",currentAfterCache.getSymbolCount(experiment.getInputAlphabet()));
                    }else{
                        batchJson.append("QueryCountAfterCache",0);
                        batchJson.append("SymbolCountAfterCache",0);
                    }
                    batchJson.append("ParallelSymbolCountAfterCache",cachedParallelSymbolCount.get(currentBatch).getSymbolCount(experiment.getInputAlphabet()));
                    batchJson.append("ParallelSymbolCount",parallelSymbolCount.get(currentBatch).getSymbolCount(experiment.getInputAlphabet()));
                    batchesJson.put(batchJson);
                }
                dataJson.append("Batches",batchesJson);

                String file_name=String.format("exp_gr%d_in%d_st%d_wk%d_it%d.dat",experimentGroup,numInputs,numStates,numWorkers,iteration);

                PrintWriter writer = new PrintWriter(path+"/"+file_name, "UTF-8");

                dataJson.write(writer);
                writer.close();
            } catch (JSONException e) {
                e.printStackTrace();
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }*/
            String file_name=String.format("exp_gr%d_in%d_st%d_wk%d_it%d_%d.dat",experimentGroup,numInputs,numStates,numWorkers,iteration,experiment.getTopCounter().size());
            try {
                PrintWriter writer = new PrintWriter(path+"/"+file_name,"UTF-8");

                GraphDOT.write(experiment.getTarget(), experiment.getInputAlphabet(), writer);
                for(BigInteger word:(List<BigInteger>)(experiment.getCounterExamples())){
                    writer.write(WordSerializer.BigintToWord(experiment.getInputAlphabet(),word).toString());
                }
                for(int currentBatchIndex=0;currentBatchIndex<experiment.getTopCounter().size();currentBatchIndex++){
                    writer.write(experiment.getTopCounter().get(currentBatchIndex).source+" ");
                    for(BigInteger word:experiment.getTopCounter().get(currentBatchIndex).wordList)
                        writer.write(WordSerializer.BigintToWord(experiment.getInputAlphabet(),word).toString());
                    writer.write("\n");
                }
                writer.close();
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        @Override
        protected void afterEveryExperiment(StatisticExperiment.StatisticExperimentDFA experiment1, StatisticExperiment.StatisticExperimentDFA experiment2) {

        }
    }
}
