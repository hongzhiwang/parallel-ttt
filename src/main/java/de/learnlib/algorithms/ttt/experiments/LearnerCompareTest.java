package de.learnlib.algorithms.ttt.experiments;

import de.learnlib.algorithms.ttt.experiments.statistics.QueryCounter;
import de.learnlib.algorithms.ttt.experiments.statistics.StatisticOracle;
import de.learnlib.algorithms.ttt.dfa.TTTLearnerDFA;
import de.learnlib.algorithms.ttt.dfa.TTTLearnerDFABuilder;
import de.learnlib.algorithms.ttt.dfa.TTTParallelLearnerDFA;
import de.learnlib.algorithms.ttt.dfa.TTTParallelLearnerDFABuilder;
import de.learnlib.api.EquivalenceOracle;
import de.learnlib.api.MembershipOracle;
import de.learnlib.eqtests.basic.IncrementalWMethodEQOracle;
import de.learnlib.oracles.DefaultQuery;
import de.learnlib.oracles.SimulatorOracle;
import net.automatalib.automata.fsa.DFA;
import net.automatalib.automata.fsa.impl.compact.CompactDFA;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Random;

/**
 * Created by Hongzhi Wang on 2015/5/19.
 */
public class LearnerCompareTest {
    public static void main(String[] args) {
        Random random = new Random();
        long diff1 = 0, diff2 = 0;
        for (int i = 0; i < 20; i++) {
            System.out.println(i);
            long[] results = compare(10, 5, random);
            diff1 += results[0] - results[2];
            diff2 += results[1] - results[3];
        }
        System.out.printf("%d  %d", diff1, diff2);
    }

    private static long[] compare(int numStates, int numInputs, Random random) {
        CompactDFA<Integer> dfa = ExperimentRunner.randomDFA(numInputs, numStates, random);
        MembershipOracle.DFAMembershipOracle<Integer> sul = new SimulatorOracle.DFASimulatorOracle<>(dfa);

        MembershipOracle<Integer, Boolean> ptttOracle = new StatisticOracle.DFAStatisticOracle<>(sul, dfa.getInputAlphabet());
        MembershipOracle<Integer, Boolean> tttOracle = new StatisticOracle.DFAStatisticOracle<>(sul, dfa.getInputAlphabet());
        TTTParallelLearnerDFA<Integer> pttt = new TTTParallelLearnerDFABuilder().withAlphabet(dfa.getInputAlphabet())
                .withOracle(ptttOracle).create();

        TTTLearnerDFA<Integer> ttt = new TTTLearnerDFABuilder<Integer>().withAlphabet(dfa.getInputAlphabet())
                .withOracle(tttOracle).create();

        EquivalenceOracle.DFAEquivalenceOracle<Integer> wMethod1 =
                new IncrementalWMethodEQOracle.DFAIncrementalWMethodEQOracle<>(dfa.getInputAlphabet(), ptttOracle, 1);

        EquivalenceOracle.DFAEquivalenceOracle<Integer> wMethod2 =
                new IncrementalWMethodEQOracle.DFAIncrementalWMethodEQOracle<>(dfa.getInputAlphabet(), tttOracle, 1);

        List<DefaultQuery<Integer, Boolean>> pCe = new ArrayList<>();
        List<DefaultQuery<Integer, Boolean>> oCe = new ArrayList<>();

        pttt.startLearning();

        boolean done = false;
        DFA hyp;
        while (!done) {
            hyp = pttt.getHypothesisModel();

            DefaultQuery<Integer, Boolean> ce = wMethod1.findCounterExample(hyp, dfa.getInputAlphabet());
            if (ce == null) {
                done = true;
                continue;
            }
            pCe.add(ce);
            pttt.refineHypothesis(ce);
        }

        ttt.startLearning();

        done = false;
        while (!done) {
            hyp = ttt.getHypothesisModel();

            DefaultQuery<Integer, Boolean> ce = wMethod2.findCounterExample(hyp, dfa.getInputAlphabet());
            if (ce == null) {
                done = true;
                continue;
            }
            oCe.add(ce);
            ttt.refineHypothesis(ce);
        }

        if(pCe.size()!=oCe.size())
            System.out.println("Size not Equal!");
        else for(int i=0;i<pCe.size();i++){
            System.out.printf("%s %s %s\n",pCe.get(i).toString(),oCe.get(i).toString(), Objects.equals(pCe.get(i),oCe.get(i))?"True":"False");
        }

        QueryCounter cpttt = ((StatisticOracle<Integer, Boolean>) ptttOracle).getCounter();
        QueryCounter cttt = ((StatisticOracle<Integer, Boolean>) tttOracle).getCounter();

        int pwl = 0, twl = 0;
        for (QueryCounter.QueryRecord record : cpttt) {
            if (record.source.contains("Equiv")) continue;
            else pwl += cpttt.getLengthCount(record);
        }
        for (QueryCounter.QueryRecord record : cttt) {
            if (record.source.contains("Equiv")) continue;
            else twl += cttt.getLengthCount(record);
        }

        System.out.printf("%d %d\n%d %d\n",cpttt.getSumWordLength(), pwl, cttt.getSumWordLength(), twl);
        return new long[]{cpttt.getSumWordLength(), pwl, cttt.getSumWordLength(), twl};

    }
}