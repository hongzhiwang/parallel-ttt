package de.learnlib.algorithms.ttt.experiments.tools;


import de.learnlib.algorithms.ttt.experiments.statistics.WordSerializer;
import net.automatalib.words.Alphabet;
import net.automatalib.words.Word;
import net.automatalib.words.impl.Alphabets;

import java.math.BigInteger;

public class SequenceBuilderTest {
    public static void main(String[] args){
        Alphabet<Character> alphabet=Alphabets.characters('a','e');
        Alphabet<Integer> alphabet1=Alphabets.integers(1, 50);
        SequenceBuilder<Character> sequenceBuilder=new SequenceBuilder<>(alphabet);
        SequenceBuilder<Integer> sequenceBuilder1=new SequenceBuilder<>(alphabet1);
        for(int i=0;i<100;i++) {
            Word<Character> word=sequenceBuilder.next();
            BigInteger bigInteger=WordSerializer.wordToBigint(alphabet, word);
            Word<Character> wordCheck=WordSerializer.BigintToWord(alphabet, bigInteger);
            // System.out.printf("%s\t%s\n", word, sequenceBuilder1.next());
            System.out.printf("%s -> %s ( %d )-> %s \n", word,bigInteger,WordSerializer.getLength(alphabet,bigInteger),wordCheck );
        }

    }
}