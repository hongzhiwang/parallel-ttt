package de.learnlib.algorithms.ttt.experiments;

import de.learnlib.algorithms.ttt.experiments.statistics.QueryCounter;
import javafx.util.Pair;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.*;

/**
 * Created by Hongzhi Wang on 2015/5/15.
 */
public class GoogleChartGenerator {
    private final String htmlFileName;
    private final StringBuffer jsCode = new StringBuffer();
    private final StringBuffer htmlBody = new StringBuffer();
    private final String template;
    private final String title;
    private final Map<String, String> colorChooser;
    private int divCounter = 0;

    public GoogleChartGenerator(String htmlFileName, String title) {
        this.htmlFileName = htmlFileName;
        this.title = title;
        template = getTemplate();
        colorChooser = getColorChooser();
    }

    private static Map<String, String> getColorChooser() {
        Map<String, String> colorChooser = new HashMap<>();
        colorChooser.put("default", "BLACK");
        colorChooser.put("startLearning", "#739E40");
        colorChooser.put("closeTransitions", "#D6DF2A");
        colorChooser.put("EquivalenceTest", "#84B819");
        colorChooser.put("prepareSplit", "#618627");
        colorChooser.put("refineHypothesisSingle", "#288C8D");
        colorChooser.put("splitState", "#94A421");
        colorChooser.put("createTransitions","#B6C930");
        colorChooser.put("updateDTTarget","BLACK");
        return colorChooser;
    }

    public void write() throws FileNotFoundException {
        PrintWriter writer = new PrintWriter(htmlFileName);
        writer.write(getHTML());
        writer.close();
        jsCode.append("google.load('visualization', '1', {packages: ['corechart', 'bar']});\n");
    }

    public String getHTML() {
        return template.replace("[#title#]", title).replace("[#script#]", jsCode.toString()).replace("[#body#]", htmlBody.toString());
    }

    public void addBarChart(List<List<Integer>> counter, Collection<Integer> roundMarker, Collection source, String title, Map<String, Integer> para) {
        String chartId = String.format("chart_%d", addDiv(title));
        StringBuffer chartTitle = new StringBuffer();
        for (String name : para.keySet()) {
            chartTitle.append(String.format("%s :%d   \\n", name, para.get(name)));
        }
        jsCode.append("draw").append(chartId).append("();");
        jsCode.append("function draw").append(chartId).append("(){");
        jsCode.append(" var data = google.visualization.arrayToDataTable([")
                .append("['Batch', 'Size', { role: 'style' }, {role:'tooltip','p': {'html': true}}],");
        int maxSize = 0;
        boolean colorChanger = false;
        int roundCounter = -1;
        int sumQuery = 0;
        for (int process = 0; process < counter.size(); process++) {
            sumQuery += counter.get(process).size();
            if (roundMarker.contains(process)) {
                colorChanger = !colorChanger;
                roundCounter++;
            }
            jsCode.append(String.format("[%d,%d,'%s','<p>Source: %s</p><p>Size: %d</p><p>Round: %s</p>']",
                            process + 1, counter.get(process).size(), colorChanger ? "dimgray" : "darkgray",
                            ((List<Pair<String, Integer>>) source).get(process).getKey(), counter.get(process).size(),
                            roundCounter >= 0 ? String.valueOf(roundCounter) : "StartLearning")
            );
            if (process != counter.size() - 1)
                jsCode.append(",\n");
            if (counter.get(process).size() > maxSize)
                maxSize = counter.get(process).size();
        }
        jsCode.append("]);\n");

        double meanSize = (double) sumQuery / counter.size();

        jsCode.append("var options={").append("title:'").append(chartTitle.toString()).append(String.format("Mean Size of Batches: %.3f", meanSize)).append("',\n");
        jsCode.append("legend:'none',");
        jsCode.append("bar: { groupWidth: '80%' },");
        jsCode.append("    tooltip: { isHtml: true },\n");
        jsCode.append("hAxis:{").append("title:'Process'},\n");
        jsCode.append("vAxis:{title:'").append("Size of batches").append("',\n");
        jsCode.append("viewWindow:{min:0, max:").append((int) (maxSize * 1.2)).append("}")
                .append("},\n");
        //jsCode.append("series:{0:{color:'dimgray'}},");
        jsCode.append("animation:{startup:true,duration:1000}");
        jsCode.append("};\n");

        jsCode.append("var chart=new google.visualization.ColumnChart(")
                .append("document.getElementById('").append(chartId).append("'));\n");
        jsCode.append("chart.draw(data, options)");
        jsCode.append("}");

/*        htmlBody.append("<div style=\"font-size:0.4em;\">");
        for(String name:para.keySet()){
            htmlBody.append("<span style=\"margin: 10px;\">").append(name).append(": ").append(para.get(name)).append("</span>");
        }
        htmlBody.append("</div>");*/
    }

    public void drawSizeOfBatches(Collection[] statistics, Map<String, Integer> para, String title) {
        StringBuffer titleSB=new StringBuffer();
        titleSB.append(title).append(" (");
        for(String name:para.keySet()){
            titleSB.append(name).append(": ").append(para.get(name));
            titleSB.append(" ");
        }
        titleSB.deleteCharAt(titleSB.length()-1);
        titleSB.append(")");

        htmlBody.append("<div class='chartContainer'>");
        htmlBody.append("<h3>").append(titleSB).append("</h3>");
        htmlBody.append("<div class='clear'></div>");
        QueryCounter cacheC = (QueryCounter) statistics[0];
        QueryCounter paralC = (QueryCounter) statistics[1];
        QueryCounter cacheT=(QueryCounter)statistics[2];
        //QueryCounter cacheT = (QueryCounter) statistics[2];
        QueryCounter paralT = (QueryCounter) statistics[3];
        Collection<Integer> cacheM = statistics[4];
        Collection<Integer> paralM = statistics[5];
        for(Collection c:statistics)
            System.out.println(c.size());
        System.out.printf("Cache Length:%d\n",cacheC.getSumWordLength());
        System.out.printf("After Cache Length:%d\n",paralC.getSumWordLength());
        drawBarChart(cacheC, cacheT, cacheM, "Direct Query", true);
        drawBarChart(cacheC, cacheT, cacheM, "Direct Query (EqTest hidden)", false);
        drawBarChart(paralC, paralT, paralM, "After Cache", true);
        drawBarChart(paralC, paralT, paralM, "After Cache (EqTest hidden)", false);
        drawPieChart(cacheC,"Distribution",true);
        drawPieChart(cacheC,"Distribution (EqTest hidden)",false);
        htmlBody.append("</div>");
        htmlBody.append("<div class='clear'></div>");
    }

    public void drawPieChart(QueryCounter counter, String title, boolean drawEq) {
        String chartId = String.format("chart_%d", addDiv(title));
        jsCode.append("draw").append(chartId).append("();");
        jsCode.append("function draw").append(chartId).append("(){");
        jsCode.append(" var data_").append(chartId)
                .append("= google.visualization.arrayToDataTable([");
        jsCode.append("['Source','Query'],");
        Map<String,Integer> sourceCount=new LinkedHashMap<>();
        for(QueryCounter.QueryRecord record:counter){
            String source=record.source;
            if(sourceCount.containsKey(source))
                sourceCount.put(source,sourceCount.get(source)+record.getQueryCount());
            else
                sourceCount.put(source,record.getQueryCount());
        }
        if(!drawEq && sourceCount.containsKey("EquivalenceTest"))
            sourceCount.remove("EquivalenceTest");
        for(String source:sourceCount.keySet()){
            jsCode.append("['").append(source).append("',").append(sourceCount.get(source)).append("],");
        }
        jsCode.append("]);\n");


        jsCode.append("var options={");
        //jsCode.append("series:{0:{color:'dimgray'}},");
        jsCode.append("pieSliceText: 'label',");
        jsCode.append("colors:[");
        for(String source:sourceCount.keySet())
            jsCode.append("'").append(colorChooser.get(source)).append("',");
        jsCode.deleteCharAt(jsCode.length()-1);
        jsCode.append("],");
        jsCode.append("animation:{startup:true,duration:1000}");
        jsCode.append("};\n");

        jsCode.append("var chart=new google.visualization.PieChart(")
                .append("document.getElementById('").append(chartId).append("'));\n");
        jsCode.append("chart.draw(data_").append(chartId)
                .append(", options)");
        jsCode.append("}");

/*        htmlBody.append("<div style=\"font-size:0.4em;\">");
        for(String name:para.keySet()){
            htmlBody.append("<span style=\"margin: 10px;\">").append(name).append(": ").append(para.get(name)).append("</span>");
        }
        htmlBody.append("</div>");*/
    }


    public void drawBarChart(QueryCounter counter, QueryCounter timer, Collection<Integer> roundMarker, String title, boolean drawEQ) {
        String chartId = String.format("chart_%d", addDiv(title));
        jsCode.append("draw").append(chartId).append("();");
        jsCode.append("function draw").append(chartId).append("(){");
        jsCode.append(" var data_").append(chartId)
                .append("= google.visualization.arrayToDataTable([")
                .append("['Batch', 'Size', { role: 'style' }, {role:'tooltip','p': {'html': true}}],");
        int maxSize = 0;
        int roundCounter = -1;
        int sumQuery = 0;

        int wordCount = 0;
        int actuWordCount=0;
        int batchCount=0;
        for (int process = 0; process < counter.size(); process++) {
            QueryCounter.QueryRecord record = counter.get(process);
            if(!(record.source=="EquivalenceTest") || drawEQ) {
                sumQuery += record.getQueryCount();
                batchCount++;
                actuWordCount+=timer.getLengthCount(timer.get(process));
                wordCount+=timer.getLengthCount(counter.get(process));
            }
            if (roundMarker.contains(process)) {
                roundCounter++;
            }
            if (roundCounter == roundMarker.size() - 1 && record.source == "EquivalenceTest") continue;
            if (!drawEQ && record.source == "EquivalenceTest") continue;
            jsCode.append(String.format("[%d,%d,'%s','<p>Source: %s</p><p>Size: %d</p><p>Round: %s</p>']",
                            batchCount, record.getQueryCount(), colorChooser.get(record.source),
                            record.source, record.getQueryCount(),
                            roundCounter >= 0 ? String.valueOf(roundCounter) : "StartLearning")
            );
            if (process != counter.size() - 1)
                jsCode.append(",\n");
            if (record.getQueryCount() > maxSize)
                maxSize = record.getQueryCount();
        }
        jsCode.append("]);\n");

        double meanSize = (double) sumQuery / batchCount;

        jsCode.append("var options={").append("title:'")
//                .append(chartTitle.toString())
                .append(String.format("Sum Length of Query Words: %d\\n", wordCount))
                .append(String.format("Parallel Length of Query Words: %d\\n", actuWordCount))
                .append(String.format("Mean Size of Batches: %.3f\\t", meanSize))
                .append(String.format("Speedup: %.3f",(double)(wordCount)/(double)(actuWordCount)))
                .append("',\n");

        jsCode.append("legend:'none',");
        jsCode.append("bar: { groupWidth: '80%' },");
        jsCode.append("    tooltip: { isHtml: true },\n");
        jsCode.append("hAxis:{").append("title:'Process'},\n");
        jsCode.append("vAxis:{title:'").append("Size of batches").append("',\n");
        jsCode.append("viewWindow:{min:0, max:").append((int) (maxSize * 1.2)).append("}")
                .append(",gridlines:{count:10},minorGridlines:{count:1}")
                .append("},\n");
        //jsCode.append("series:{0:{color:'dimgray'}},");
        jsCode.append("animation:{startup:true,duration:1000}");
        jsCode.append("};\n");

        jsCode.append("var chart=new google.visualization.ColumnChart(")
                .append("document.getElementById('").append(chartId).append("'));\n");
        jsCode.append("chart.draw(data_").append(chartId)
                .append(", options)");
        jsCode.append("}");

/*        htmlBody.append("<div style=\"font-size:0.4em;\">");
        for(String name:para.keySet()){
            htmlBody.append("<span style=\"margin: 10px;\">").append(name).append(": ").append(para.get(name)).append("</span>");
        }
        htmlBody.append("</div>");*/
    }

    public void addLineChart(Map<Integer, Double> statistics, String title, String xAxis, Map<String, Integer> para, boolean showLine) {
        String chartId = String.format("chart_%d", addDiv(title));
        StringBuffer chartTitle = new StringBuffer();
        for (String name : para.keySet()) {
            chartTitle.append(String.format("%s :%d   \\n", name, para.get(name)));
        }
        jsCode.append("draw").append(chartId).append("();");
        jsCode.append("function draw").append(chartId).append("(){");

        jsCode.append(" var data = google.visualization.arrayToDataTable([")
                .append(String.format("['%s','Mean size'],", xAxis));
        int pointCounter = 0;
        double maxY = 0;
        for (int x : statistics.keySet()) {
            double y = statistics.get(x);
            jsCode.append(String.format("[%d,%f]", x, y));
            if (++pointCounter < statistics.size()) jsCode.append(",");
            if (y > maxY) maxY = y;
        }
        jsCode.append("]);\n");
        jsCode.append("var options={").append("title:'").append(chartTitle.toString()).append("',\n");
        jsCode.append("legend:'none',");
        //jsCode.append("    tooltip: { isHtml: true },\n");
        jsCode.append("hAxis:{").append(String.format("title:'%s',viewWindow:{min:0}},\n", xAxis));
        jsCode.append("vAxis:{title:'").append("Mean size of batches").append("',\n");
        jsCode.append("viewWindow:{min:0, max:").append((int) (maxY * 1.2)).append("}")
                .append("},\n");
        jsCode.append("pointSize:10,");
        jsCode.append("series:{0:{color:'dimgray',pointShape:'square'}},");
        jsCode.append("animation:{startup:true,duration:1000}");
        jsCode.append("};\n");

        jsCode.append("var chart=new google.visualization.").append(showLine ? "Line" : "Scatter").append("Chart(")
                .append("document.getElementById('").append(chartId).append("'));\n");
        jsCode.append("chart.draw(data, options)");
        jsCode.append("}");

    }

    private String getTemplate() {
        StringBuffer templateBuilder = new StringBuffer();
        templateBuilder.append("<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.01//EN\"\n")
                .append("   \"http://www.w3.org/TR/html4/strict.dtd\">\n")
                .append("<HTML>\n")
                .append("   <HEAD>\n")
                .append("      <TITLE>[#title#]</TITLE>\n")
                .append("      <script type=\"text/javascript\" src=\"https://www.google.com/jsapi?autoload={'modules':[{'name':'visualization','version':'1.1','packages':['corechart']}]}\"></script>")
                .append("<script type=\"text/javascript\" src=\"http://ajax.googleapis.com/ajax/libs/jquery/1.4/jquery.min.js\"></script>")
                .append("    <link rel=\"stylesheet\" type=\"text/css\" href=\"exampleStyle.css\">")
                .append("      <script type=\"text/javascript\">\n")
                .append("      google.setOnLoadCallback(drawChart);\n")
                .append("      function drawChart() {")
                .append("[#script#]}")
                .append("</script>")
                .append("<style>")
                .append(".collapsed-table tr {\n")
                .append("  display: none;\n")
                .append("}")
                .append("</style>")
                .append("   </HEAD>\n")
                .append("   <BODY>\n")
                .append("<h1>[#title#]</h1>")
                .append("[#body#]")
                .append("   </BODY>\n")
                .append("</HTML>");
        return templateBuilder.toString();
    }

    private int addDiv(String subtitle) {
        htmlBody.append("<div class='subChartContainer'>");
        htmlBody.append("<h4 id=\"title_").append(divCounter).append("\">").append(subtitle).append("</h4><div id=\"chart_").append(divCounter).append("\" class='subChart'></div>");
        htmlBody.append("</div>");
        return divCounter++;
    }
}
