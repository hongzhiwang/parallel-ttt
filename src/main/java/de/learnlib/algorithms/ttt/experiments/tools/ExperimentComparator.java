package de.learnlib.algorithms.ttt.experiments.tools;

import de.learnlib.api.EquivalenceOracle;
import de.learnlib.api.LearningAlgorithm;
import de.learnlib.api.MembershipOracle;
import net.automatalib.automata.Automaton;
import net.automatalib.automata.fsa.DFA;
import net.automatalib.automata.fsa.impl.compact.CompactDFA;
import net.automatalib.words.Alphabet;
import net.automatalib.words.impl.Alphabets;

import java.util.Random;

/**
 * This Class compares two different configurations of learning experiments.
 * To use this class: just inherit the DFA or Mealy one and override the abstract methods
 *
 * @param <A>
 * @param <I>
 * @param <D>
 * @param <SE>
 */
public abstract class ExperimentComparator<A extends Automaton, I, D, SE extends StatisticExperiment> {
    protected final int numStatesStart, numStatesEnd;
    protected final int numInputsStart, numInputsEnd;
    protected final int numOutputsStart, numOutputsEnd;
    protected final int numWorkersStart, numWorkersEnd;
    protected final int numIterations;
    protected final Random random;
    protected final OracleChain.CachePolicy cachePolicy1, cachePolicy2;

    protected LearningAlgorithm<A, I, D> learner1, learner2;
    protected EquivalenceOracle<A, I, D> eqOracle1, eqOracle2;
    protected SE experimenter1, experimenter2;


    /**
     * Defines range of these parameters. the comparator will iterate through every combination.
     * Note: end number must greater or equal to the start number, and the step of iterations is always 1.
     */
    protected ExperimentComparator(int numStatesStart, int numStatesEnd, int numInputsStart, int numInputsEnd, int numOutputsStart, int numOutputsEnd, int numWorkersStart, int numWorkersEnd, int numIterations, Random random, OracleChain.CachePolicy cachePolicy1, OracleChain.CachePolicy cachePolicy2) {
        this.numStatesStart = numStatesStart;
        this.numStatesEnd = numStatesEnd;
        this.numInputsStart = numInputsStart;
        this.numInputsEnd = numInputsEnd;
        this.numOutputsStart = numOutputsStart;
        this.numOutputsEnd = numOutputsEnd;
        this.numWorkersStart = numWorkersStart;
        this.numWorkersEnd = numWorkersEnd;
        this.numIterations = numIterations;

        this.random = random;
        this.cachePolicy1 = cachePolicy1;
        this.cachePolicy2 = cachePolicy2;
    }

    public void run() {
        for (int numInputs = numInputsStart; numInputs <= numInputsEnd; numInputs++) {
            for (int numStates = numStatesStart; numStates <= numStatesEnd; numStates++) {
                for (int numOutputs = numOutputsStart; numOutputs <= numOutputsEnd; numOutputs++) {
                    for (int numWorkers = numWorkersStart; numWorkers <= numWorkersEnd; numWorkers++) {
                        for (int iteration = 1; iteration <= numIterations; iteration++) {
                            runExperiment(numInputs, numStates, numOutputs, numWorkers, iteration);
                        }
                    }
                }
            }
        }
        afterRun();
    }

    protected void afterRun(){}

    protected abstract void runExperiment(int numInputs, int numStates, int numOutputs, int numWorkers, int iteration);

    public abstract static class ExperimentComparatorDFA<
            L1 extends LearningAlgorithm<DFA<?, Integer>, Integer, Boolean>, L2 extends LearningAlgorithm<DFA<?, Integer>, Integer, Boolean>,
            EQ1 extends EquivalenceOracle<DFA<?, Integer>, Integer, Boolean>, EQ2 extends EquivalenceOracle<DFA<?, Integer>, Integer, Boolean>>
            extends ExperimentComparator<DFA<?, Integer>, Integer, Boolean, StatisticExperiment.StatisticExperimentDFA<Integer>> {


        /**
         * Defines range of these parameters. the comparator will iterate through every combination.
         * Note: end number must greater or equal to the start number, and the step of iterations is always 1.
         */
        protected ExperimentComparatorDFA(int numStatesStart, int numStatesEnd, int numInputsStart, int numInputsEnd, int numOutputsStart, int numOutputsEnd, int numWorkersStart, int numWorkersEnd, int numIterations, Random random, OracleChain.CachePolicy cachePolicy1, OracleChain.CachePolicy cachePolicy2) {
            super(numStatesStart, numStatesEnd, numInputsStart, numInputsEnd, numOutputsStart, numOutputsEnd, numWorkersStart, numWorkersEnd, numIterations, random, cachePolicy1, cachePolicy2);
        }


        /**
         * Override these methods to define learners.
         * The reason that the constructor methods are not directly called is that the constructing process might
         * involve builder classes and different parameters might be required for different constructors.
         *
         * @param alphabet
         * @param oracle
         * @return
         */
        protected abstract L1 defineLearner1(Alphabet<Integer> alphabet, MembershipOracle<Integer, Boolean> oracle);

        protected abstract L2 defineLearner2(Alphabet<Integer> alphabet, MembershipOracle<Integer, Boolean> oracle);

        /**
         * Override these methods to define equivalence oracles.
         *
         * @param alphabet
         * @param oracle
         * @param numWorkers
         * @return
         */
        protected abstract EQ1 defineEquivalenceOracle1(Alphabet<Integer> alphabet, MembershipOracle<Integer, Boolean> oracle, int numWorkers);

        protected abstract EQ2 defineEquivalenceOracle2(Alphabet<Integer> alphabet, MembershipOracle<Integer, Boolean> oracle, int numWorkers);

        /**
         * Override this method to record the results.
         *
         * @param experiment
         */
        protected abstract void saveExperimentResults(StatisticExperiment.StatisticExperimentDFA<Integer> experiment, int experimentGroup, int numInputs, int numStates, int numOutputs, int numWorkers, int iteration, String automatType);

        protected abstract void afterEveryExperiment(StatisticExperiment.StatisticExperimentDFA experiment1, StatisticExperiment.StatisticExperimentDFA experiment2);

        @Override
        protected void runExperiment(int numInputs, int numStates, int numOutputs, int numWorkers, int iteration) {
            Alphabet<Integer> alphabet = Alphabets.integers(0, numInputs-1);
            CompactDFA<Integer> target = AutomataHelper.randomDFA(alphabet, numStates, random);
            StatisticExperiment.StatisticExperimentDFA<Integer> experiment1 =
                    new StatisticExperiment.StatisticExperimentDFA<>(target, alphabet, cachePolicy1, numWorkers);
            StatisticExperiment.StatisticExperimentDFA<Integer> experiment2 =
                    new StatisticExperiment.StatisticExperimentDFA<>(target, alphabet, cachePolicy2, numWorkers);

            learner1 = defineLearner1(alphabet, experiment1.getTopOracle());
            learner2 = defineLearner2(alphabet, experiment2.getTopOracle());

            eqOracle1 = defineEquivalenceOracle1(alphabet, experiment1.getTopOracle(), numWorkers);
            eqOracle2 = defineEquivalenceOracle2(alphabet, experiment2.getTopOracle(), numWorkers);

            experiment1.setLearner(learner1);
            experiment2.setLearner(learner2);

            experiment1.setEqOracle(eqOracle1);
            experiment2.setEqOracle(eqOracle2);

            experiment1.run();
            saveExperimentResults(experiment1, 1, numInputs, numStates, numOutputs, numWorkers, iteration, "DFA");

            experiment2.run();
            saveExperimentResults(experiment2, 2, numInputs, numStates, numOutputs, numWorkers, iteration, "DFA");

            afterEveryExperiment(experiment1, experiment2);
        }
    }
}
