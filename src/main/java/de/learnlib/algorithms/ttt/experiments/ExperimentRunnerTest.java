package de.learnlib.algorithms.ttt.experiments;

import de.learnlib.algorithms.ttt.dfa.TTTParallelLearnerDFA;
import de.learnlib.algorithms.ttt.dfa.TTTParallelLearnerDFABuilder;
import de.learnlib.algorithms.ttt.experiments.statistics.QueryCounter;
import de.learnlib.algorithms.ttt.experiments.tools.OracleChain;
import de.learnlib.algorithms.ttt.experiments.tools.StatisticExperiment;
import de.learnlib.parallelism.ParallelIncrementalWMethodEQOracle;
import net.automatalib.words.Alphabet;
import net.automatalib.words.impl.Alphabets;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Random;

public class ExperimentRunnerTest {

    static public void testRunDFA() throws Exception {
        ExperimentRunner.ExperimentSetting setting = new ExperimentRunner.ExperimentSetting(8, 5, 3, 10, new Random());
        Collection[] results = ExperimentRunner.runDFA(setting);
//        for(QueryRecord record:(QueryCounter)results[0]){
//            System.out.println(record.source);
//        }

        String testFileName = "test_gchart.html";
        GoogleChartGenerator gChart = new GoogleChartGenerator(testFileName, "Experiment visualization");
        Map<String, Integer> para = new LinkedHashMap<>();
        para.put("States", setting.numState);
        para.put("Inputs", setting.numInput);
        para.put("Workers", setting.numWorker);

        //gChart.drawBarChart((QueryCounter)results[1],(QueryCounter)results[2],results[4],"DFA",para,false);
        gChart.drawSizeOfBatches(results, para, "Size of Batches DFA");

        para.put("Outputs", setting.numOutput);
        results = ExperimentRunner.runMealy(setting);
        gChart.drawSizeOfBatches(results, para, "Size of Batches Mealy");

        gChart.write();

        String urlStr = "File:///" + System.getProperty("user.dir") + File.separator + testFileName;
        ExperimentRunner.showPage(urlStr);
    }

    static public void testRunDFAnewClass() throws IOException, URISyntaxException {
        Alphabet<Integer> alphabet = Alphabets.integers(0, 5);

        StatisticExperiment.StatisticExperimentDFA<Integer> experimentDFA =
                new StatisticExperiment.StatisticExperimentDFA<>(5, 8, alphabet, OracleChain.CachePolicy.FillCache, new Random());
        TTTParallelLearnerDFA<Integer> learnerDFA =
                new TTTParallelLearnerDFABuilder<Integer>().withAlphabet(alphabet).withOracle(experimentDFA.getTopOracle()).create();
        ParallelIncrementalWMethodEQOracle.DFAParallelIncrementalWMethodEQOracle<Integer> equivalenceOracle =
                new ParallelIncrementalWMethodEQOracle.DFAParallelIncrementalWMethodEQOracle<>(alphabet, experimentDFA.getTopOracle(), 3, 20);
        experimentDFA.setLearner(learnerDFA);
        experimentDFA.setEqOracle(equivalenceOracle);

        experimentDFA.run();

        ExperimentRunner.ExperimentSetting setting =
                new ExperimentRunner.ExperimentSetting(8, 5, 3, 10, new Random());
        QueryCounter parallelWordCount =
                QueryCounter.parallelWordCount(experimentDFA.getParallelCounter(), experimentDFA.getWorkerCounters(), alphabet);
        QueryCounter cacheWordCount =
                QueryCounter.parallelWordCount(experimentDFA.getTopCounter(), experimentDFA.getShadowCounters(), alphabet);

        System.out.println();
        Collection topCounter = experimentDFA.getTopCounter();
        Collection parallelCounter = experimentDFA.getParallelCounter();
        Collection[] results = {topCounter, parallelCounter, cacheWordCount, parallelWordCount, experimentDFA.getRoundMarkerTop(), experimentDFA.getRoundMarkerParallel()};
//        for(QueryRecord record:(QueryCounter)results[0]){
//            System.out.println(record.source);
//        }

        String testFileName = "test_gchart.html";
        GoogleChartGenerator gChart = new GoogleChartGenerator(testFileName, "Experiment visualization");
        Map<String, Integer> para = new LinkedHashMap<>();
        para.put("States", setting.numState);
        para.put("Inputs", setting.numInput);
        para.put("Workers", setting.numWorker);

        //gChart.drawBarChart((QueryCounter)results[1],(QueryCounter)results[2],results[4],"DFA",para,false);
        gChart.drawSizeOfBatches(results, para, "Size of Batches DFA");

        para.put("Outputs", setting.numOutput);

        gChart.write();

        String urlStr = "File:///" + System.getProperty("user.dir") + File.separator + testFileName;
        ExperimentRunner.showPage(urlStr);
    }

    public static void main(String[] args) throws Exception {
        //testRunDFA();
        testRunDFAnewClass();
    }
}