package de.learnlib.algorithms.ttt.experiments.tools;


import net.automatalib.words.Alphabet;
import net.automatalib.words.Word;

import java.util.ArrayList;
import java.util.List;

public class SequenceBuilder<I> {
    private final Alphabet<I> alphabet;
    private List<Integer> wordDigit =new ArrayList<>();

    public SequenceBuilder(Alphabet<I> alphabet){
        this.alphabet=alphabet;
    }

    public Word<I> next(){
        increment();
        return buildWord();
    }

    private void increment(){
        if(wordDigit.size()==0){
            wordDigit.add(0);
            return;
        }
        int curr=0;
        int maxLetter=alphabet.size()-1;
        while(true){
            if(wordDigit.get(curr)<maxLetter){
                wordDigit.set(curr,wordDigit.get(curr)+1);
                return;
            }
            wordDigit.set(curr,0);
            if(wordDigit.size()<=curr+1){
                wordDigit.add(0);
                return;
            }
            curr+=1;
        }
    }

    private Word<I> buildWord(){
        List<I> letterList=new ArrayList<>();
        for(int i=wordDigit.size()-1;i>=0;i--){
            letterList.add(alphabet.getSymbol(wordDigit.get(i)));
        }
        return Word.fromList(letterList);
    }
}
