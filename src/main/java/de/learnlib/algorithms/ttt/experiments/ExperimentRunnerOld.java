/*
package de.learnlib.algorithms.ttt.Experiments;

import com.googlecode.charts4j.*;
import com.googlecode.charts4j.Color;
import com.googlecode.charts4j.Shape;
import de.learnlib.algorithms.lstargeneric.mealy.ClassicLStarMealy;
import de.learnlib.algorithms.lstargeneric.mealy.ClassicLStarMealyBuilder;
import de.learnlib.algorithms.ttt.dfa.TTTLearnerDFA;
import de.learnlib.algorithms.ttt.dfa.TTTLearnerDFABuilder;
import de.learnlib.algorithms.ttt.dfa.TTTParallelLearnerDFA;
import de.learnlib.algorithms.ttt.dfa.TTTParallelLearnerDFABuilder;
import de.learnlib.algorithms.ttt.mealy.TTTLearnerMealy;
import de.learnlib.algorithms.ttt.mealy.TTTLearnerMealyBuilder;
import de.learnlib.algorithms.ttt.mealy.TTTParallelLearnerMealy;
import de.learnlib.algorithms.ttt.mealy.TTTParallelLearnerMealyBuilder;
import de.learnlib.api.EquivalenceOracle;
import de.learnlib.api.LearningAlgorithm;
import de.learnlib.api.MembershipOracle;
import de.learnlib.cache.dfa.DFACacheOracle;
import de.learnlib.cache.dfa.DFACaches;
import de.learnlib.cache.mealy.MealyCacheOracle;
import de.learnlib.cache.mealy.MealyCaches;
import de.learnlib.eqtests.basic.IncrementalWMethodEQOracle;
import de.learnlib.eqtests.basic.RandomWordsEQOracle;
import de.learnlib.eqtests.basic.SimulatorEQOracle;
import de.learnlib.oracles.DefaultQuery;
import de.learnlib.oracles.SimulatorOracle;
import de.learnlib.parallelism.ParallelCompleteExplorationEQOracle.DFAParallelCompleteExplorationEQQracle;
import de.learnlib.parallelism.*;
import de.learnlib.statistics.Counter;
import javafx.util.Pair;
import net.automatalib.automata.MutableAutomaton;
import net.automatalib.automata.fsa.DFA;
import net.automatalib.automata.fsa.impl.compact.CompactDFA;
import net.automatalib.automata.transout.MealyMachine;
import net.automatalib.automata.transout.impl.compact.CompactMealy;
import net.automatalib.commons.dotutil.DOT;
import net.automatalib.incremental.ConflictException;
import net.automatalib.util.automata.copy.AutomatonCopyMethod;
import net.automatalib.util.automata.copy.AutomatonLowLevelCopy;
import net.automatalib.util.automata.random.RandomAutomata;
import net.automatalib.util.graphs.dot.GraphDOT;
import net.automatalib.words.Alphabet;
import net.automatalib.words.Word;
import net.automatalib.words.impl.Alphabets;

import java.awt.*;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.Writer;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.*;
import java.util.List;

import static com.googlecode.charts4j.Color.*;


public class ExperimentRunnerOld {

    public static void main(String[] args) throws IOException, URISyntaxException {

*/
/*        Map<String, String> urlList = new LinkedHashMap<>();
        String[] urls = meanBatch(new Random());
        urlList.put("Mean Batch Size with Different number of States", urls[0]);
        urlList.put("Mean Batch Size with Different number of Inputs", urls[1]);
        urlList.put("Size of Batches (Mealy Machine)", sizeOfBatchMealy(5, 10, 4, 16, false, false));
        urlList.put("Size of Batches (DFA)", sizeOfBatchDFA(10, 10, 16, false, "test_out_4.output"));

        String htmlName = "output.html";
        PrintWriter htmlWriter = new PrintWriter(htmlName);
        writeHTML(htmlWriter, urlList);
        System.out.println(System.getProperty("user.dir") + File.separator + htmlName);
        showChart("FILE:///"+System.getProperty("user.dir") + File.separator + htmlName);*//*


        //runMealy(4,4,4,4,new Random(),true,true);
        //runDFA(4, 4, 4, null, true, new Random(), true, true);

        ExperimentSettings settings = new ExperimentSettings(1, 2, 3, 4, true, true, ExperimentSettings.AutomataType.DFA, new Random());
        System.out.println(settings.getAutomataType());
    }

    static Collection[] runExperiment(ExperimentSettings settings) {
        Alphabet<Integer> sigma = Alphabets.integers(0, settings.getNumInput() - 1);
        Object[] inputs = new Integer[settings.numInput];
        sigma.toArray(inputs);

        MutableAutomaton origin = null;

        List<MembershipOracle> oracles = new ArrayList<>();

        switch (settings.getAutomataType()) {
            case DFA:
                CompactDFA<Integer> dfa = RandomAutomata.randomDFA(settings.getRandom(), settings.getNumState(), sigma, true);
                origin = dfa;
                for (int i = 0; i < settings.getNumWorker(); i++) {
                    CompactDFA<Integer> target = cloneDFA(dfa);
                    MembershipOracle<Integer, Boolean> sul = new SimulatorOracle.DFASimulatorOracle<>(target);
                    MembershipOracle<Integer, Boolean> oracle = new StatisticOracle(sul);
                    oracles.add(oracle);
                }
                break;
            case MEALY:
                Alphabet<Character> lambda = Alphabets.characters('a', Character.toChars(('a' + settings.getNumOutput()) - 1)[0]);
                Character[] outputs = new Character[settings.getNumOutput()];
                lambda.toArray(outputs);
                MealyMachine<?, Integer, ?, Character> mealy = RandomAutomata.randomMealy(settings.getRandom(), settings.getNumState(), sigma, Arrays.asList(outputs), true);
                origin = (MutableAutomaton) mealy;
                for (int i = 0; i < settings.getNumWorker(); i++) {
                    CompactMealy<Integer,Character> clone = new CompactMealy<>(sigma, settings.getNumState());
                    AutomatonLowLevelCopy.copy(AutomatonCopyMethod.DFS, origin, Arrays.asList(inputs), clone);
                    MembershipOracle<Integer,Character> sul=new SimulatorOracle.MealySimulatorOracle<>((MealyMachine) clone);

                    MembershipOracle<Integer, Character> oracle = new StatisticOracle(sul);
                    oracles.add(oracle);
                }
                break;
            StatisticStaticParallelOracle<> parallelOracle=new StatisticStaticParallelOracle(oracles,1, ParallelOracle.PoolPolicy.FIXED);
        }

        return null;
    }

    static Collection[] runMealy(int num_input, int num_output, int num_state, int num_worker, Random random, Boolean showGraph, Boolean countCache) {


        Alphabet<Integer> sigma = Alphabets.integers(0, num_input - 1);
        Integer[] inputs = new Integer[num_input];
        sigma.toArray(inputs);

        Alphabet<Character> lambda = Alphabets.characters('a', Character.toChars(('a' + num_output) - 1)[0]);

        Character[] outputs = new Character[num_output];
        lambda.toArray(outputs);

        MealyMachine<?, Integer, ?, Character> origin = RandomAutomata.randomMealy(random, num_state, sigma, Arrays.asList(outputs), true);
        CompactMealy<Integer, Character>[] target_clone = new CompactMealy[num_worker];
        List<MembershipOracle<Integer, Word<Character>>> oracles = new ArrayList<>();

        for (int i = 0; i < num_worker; i++) {
            target_clone[i] = new CompactMealy<Integer, Character>(sigma, num_state);
            AutomatonLowLevelCopy.copy(AutomatonCopyMethod.DFS, origin, Arrays.asList(inputs), target_clone[i]);
            oracles.add(new SimulatorOracle.MealySimulatorOracle<Integer, Character>(target_clone[i]));
        }
        MembershipOracle<Integer, Word<Character>> pOracle = new StaticParallelOracle<>(oracles, 1, ParallelOracle.PoolPolicy.FIXED);

        StatisticOracle<Integer, Word<Character>> spOracle = new StatisticOracle<>(pOracle, true);

        MealyCacheOracle<Integer, Character> cacheOracle = MealyCaches.createCache(sigma, spOracle);

        StatisticOracle<Integer, Word<Character>> scOracle = new StatisticOracle<>(cacheOracle, true);


        TTTParallelLearnerOldMealy<Integer, Character> tttm =
                new TTTParallelLearnerMealyBuilder<Integer, Character>()
                        .withBatchSize(num_worker)
                        .withAlphabet(sigma)
                        .withOracle(scOracle)
                        .create();

        TTTLearnerMealy<Integer, Character> ttt = new TTTLearnerMealyBuilder().withAlphabet(sigma).withOracle(scOracle).create();

        ClassicLStarMealy lStarMealy = new ClassicLStarMealyBuilder().withAlphabet(sigma).withOracle(scOracle).create();

        ParallelIncrementalWMethodEQOracle.MealyParallelIncrementalWMethodEQOracle<Integer, Character> pinc = null;
        pinc = new ParallelIncrementalWMethodEQOracle.MealyParallelIncrementalWMethodEQOracle<>(sigma, scOracle, 2, num_worker);


        LearningAlgorithm.MealyLearner<Integer, Character> learner = tttm;
        EquivalenceOracle.MealyEquivalenceOracle<Integer, Character> equivalenceOracle = pinc;
        StatisticOracle<Integer, Word<Character>> counterOracle = countCache ? scOracle : spOracle;

        List<Integer> cRoundMarker = new ArrayList<>();
        List<Integer> pRoundMarker = new ArrayList<>();
        List<Integer> roundMarker = new ArrayList<>();

        try {
            learner.startLearning();
            int round = 0;

            boolean done = false;
            MealyMachine<?, Integer, ?, Character> hyp = null;
            while (!done) {
                hyp = learner.getHypothesisModel();
                roundMarker.add(counterOracle.getQueryCount().size());
                cRoundMarker.add(scOracle.getQueryCount().size());
                pRoundMarker.add(spOracle.getQueryCount().size());
                DefaultQuery<Integer, Word<Character>> ce = equivalenceOracle.findCounterExample(hyp, sigma);
                if (ce == null) {
                    done = true;
                    continue;
                }

                learner.refineHypothesis(ce);
            }
        } catch (ConflictException e) {
            return null;
        } finally {
            ((StaticParallelOracle) pOracle).shutdownNow();
            if (showGraph) {
                try {

                    Writer w = DOT.createDotWriter(true);
                    GraphDOT.write(origin, sigma, w);
                    w.close();
                    w = DOT.createDotWriter(true);
                    GraphDOT.write(target_clone[0], sigma, w);
                    w.close();
                    w = DOT.createDotWriter(true);
                    GraphDOT.write(learner.getHypothesisModel(), sigma, w);
                    w.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        List<List<Integer>> statistic = counterOracle.getQueryCount();

        List<Pair<String, Integer>> source = counterOracle.getSourceCounter();

        return new Collection[]{statistic, roundMarker, source};
    }

    static String[] meanBatch(Random random) throws IOException, URISyntaxException {
        int maxOrder = 8, num_input = 4, num_worker = 16, num_output = 8, experimentsEach = 2;
        boolean countCache = true;
        Map<Integer, Double> meanSize = null;
        meanSize = runMeanBatchState(maxOrder, num_input, num_worker, num_output, random, experimentsEach, countCache);
        Map<String, Integer> para = new LinkedHashMap<>();
        para.put("Inputs", num_input);
        para.put("Outputs", num_output);
        para.put("Workers", num_worker);
        String url1 = linechart(meanSize, "Number of states", para);
//        showChart(url1);
        int num_state = 10;
        num_output = 4;
        maxOrder = 7;
        para = new LinkedHashMap<>();
        para.put("States", num_state);
        para.put("Outputs", num_output);
        para.put("Workers", num_worker);
        meanSize = runMeanBatchInput(maxOrder, num_state, num_worker, num_output, random, experimentsEach, countCache);
        String url2 = linechart(meanSize, "Number of inputs", para);
//        showChart(url2);
        return new String[]{url1, url2};
    }

    static String sizeOfBatchMealy(int num_input, int num_state, int num_output, int num_worker, boolean countEQ, boolean verbose) throws IOException, URISyntaxException {
        Collection[] lists = runMealy(num_input, num_output, num_state, num_worker, new Random(), false, !countEQ);
        Map<String, Integer> parameterMap = new LinkedHashMap<>();
        parameterMap.put("Input", num_input);
        parameterMap.put("Output", num_output);
        parameterMap.put("States", num_state);
        String urlStr = barchart(lists[0], lists[1], parameterMap);
//        showChart(urlStr);
        if (verbose) {
            int batchCount = 0;
            int roundCount = 0;
            for (Pair<String, Integer> batch : (List<Pair<String, Integer>>) lists[2]) {
                if (lists[1].contains(batchCount)) System.out.format("***** Round %d *****\n", roundCount++);
                System.out.printf("[%s] : %d\n", batch.getKey(), batch.getValue());
            }
        }
        return urlStr;
    }

    static String sizeOfBatchDFA(int num_input, int num_state, int num_worker, boolean countEQ, String filename) throws IOException, URISyntaxException {
        PrintWriter writer = new PrintWriter(filename);
        Collection[] lists = runDFA(num_input, num_state, num_worker, writer, countEQ, new Random(), false, true);
        Map<String, Integer> parameterMap = new LinkedHashMap<>();
        parameterMap.put("Input", num_input);
        parameterMap.put("States", num_state);
        String urlStr = barchart(lists[0], lists[1], parameterMap);
//        showChart(urlStr);
        return urlStr;
    }

    static Map<Integer, Double> runMeanBatchState(int maxOrder, int num_input, int num_worker, int num_output, Random random, int experimentsEach, Boolean countCache) {
        int num_state = 1;
        Map<Integer, Double> results = new LinkedHashMap<>();
        for (int order = 1; order <= maxOrder; order++) {
            Counter sumBatchSize = new Counter("sum batch size", "");
            int batchCount = 0;
            num_state *= 2;
            System.out.format("********* Number of State: %d ********\n", num_state);
            for (int experiment = 0; experiment < experimentsEach; experiment++) {
                //System.out.printf("Experiment: %d\n", experiment);
                Collection[] result = runMealy(num_input, num_state, num_output, num_worker, random, false, countCache);
                if (result == null) {
                    System.err.println("Conflict");
                    continue;
                }
                Collection<List<Integer>> batchList = result[0];
                batchCount += batchList.size();
                for (List<Integer> batch : batchList) {
                    sumBatchSize.increment(batch.size());
                }

//                Collection<List<Integer>> caList = result[0];
//                Collection<List<Integer>> paList = result[2];
//                int caSum = caList.stream().mapToInt(t -> t.size()).sum();
//                int paSum = paList.stream().mapToInt(t -> t.size()).sum();
//                System.out.printf("%d[%d], cache: %d:%f , direct: %d:%f\n", numState, experiment, caSum, (double) caSum / caList.size(), paSum, (double) paSum / paList.size());

                results.put(num_state, (double) sumBatchSize.getCount() / batchCount);
            }
            System.out.printf("states: %d, batch count: %d, query count: %d, average batch size: %f\n", num_state, batchCount, sumBatchSize.getCount(), (double) (sumBatchSize.getCount()) / batchCount);
        }
        return results;
    }

    static Map<Integer, Double> runMeanBatchInput(int maxOrder, int num_state, int num_worker, int num_output, Random random, int experimentsEach, Boolean countCache) {
        int num_input = 1;
        Map<Integer, Double> results = new LinkedHashMap<>();
        for (int order = 1; order <= maxOrder; order++) {
            Counter sumBatchSize = new Counter("sum batch size", "");
            int batchCount = 0;
            num_input *= 2;
            System.out.format("********* Number of Input: %d ********\n", num_input);
            for (int experiment = 0; experiment < experimentsEach; experiment++) {
                //System.out.printf("Experiment: %d\n", experiment);
                Collection[] result = runMealy(num_state, num_input, num_output, num_worker, random, false, countCache);
                if (result == null) {
                    System.err.println("Conflict");
                    continue;
                }
                Collection<List<Integer>> batchList = result[0];
                batchCount += batchList.size();
                for (List<Integer> batch : batchList) {
                    sumBatchSize.increment(batch.size());
                }

//                Collection<List<Integer>> caList = result[0];
//                Collection<List<Integer>> paList = result[2];
//                int caSum = caList.stream().mapToInt(t -> t.size()).sum();
//                int paSum = paList.stream().mapToInt(t -> t.size()).sum();
//                System.out.printf("%d[%d], cache: %d:%f , direct: %d:%f\n", num_input, experiment, caSum, (double) caSum / caList.size(), paSum, (double) paSum / paList.size());

                results.put(num_input, (double) sumBatchSize.getCount() / batchCount);
            }
            System.out.printf("states: %d, batch count: %d, query count: %d, average batch size: %f\n", num_input, batchCount, sumBatchSize.getCount(), (double) (sumBatchSize.getCount()) / batchCount);
        }
        return results;
    }

    public static Collection[] runDFA(int num_input, int num_state, int num_worker, PrintWriter writer, boolean countEQ, Random random, boolean showGraph, boolean randomDFA) {
        boolean countCache = false;
        Alphabet<Integer> inputs = null;
        List<MembershipOracle.DFAMembershipOracle<Integer>> oracles = new ArrayList<>();
        CompactDFA<Integer> targetOriginal = null;
        if (randomDFA)
            targetOriginal = randomDFA(num_state, num_input, random);
        else
            targetOriginal = constructSumDFA(num_state, num_input, num_state - 1, false);

        inputs = targetOriginal.getInputAlphabet();
        for (int i = 0; i < num_worker; i++) {
            CompactDFA<Integer> target = cloneDFA(targetOriginal);
            MembershipOracle.DFAMembershipOracle<Integer> sul = new SimulatorOracle.DFASimulatorOracle<>(target);
            oracles.add(sul);
        }
//        StatisticStaticParallelOracle<Integer, Boolean> parallelOracle =
//                new StatisticStaticParallelOracle<>(oracles, 1, StatisticStaticParallelOracle.PoolPolicy.FIXED);
        ParallelOracle<Integer, Boolean> parallelOracle = new StaticParallelOracle<>(oracles, 1, ParallelOracle.PoolPolicy.FIXED);
        StatisticOracle.DFAStatisticOracle<Integer> statisticParallelOracle = new StatisticOracle.DFAStatisticOracle<>(parallelOracle, !countEQ);

        DFACacheOracle<Integer> cacheOracle = DFACaches.createCache(inputs, statisticParallelOracle);

        StatisticOracle.DFAStatisticOracle<Integer> statisticCacheOracle = new StatisticOracle.DFAStatisticOracle<>(cacheOracle, !countEQ);

        TTTParallelLearnerOldDFA tttpc = new TTTParallelLearnerDFABuilder<Integer>()
                .withAlphabet(inputs)
                .withOracle(statisticCacheOracle)
                .withBatchSize(num_worker)
                .create();

        TTTLearnerDFA<Integer> ttt = new TTTLearnerDFABuilder<Integer>()
                .withAlphabet(inputs)
                .withOracle(statisticCacheOracle)
                .create();


        RandomWordsEQOracle.DFARandomWordsEQOracle<Integer> randomWordsEQOracle = null;
        DFAParallelCompleteExplorationEQQracle<Integer> ceq = null;
        IncrementalWMethodEQOracle.DFAEquivalenceOracle<Integer> increw = null;
        ParallelIncrementalWMethodEQOracle.DFAParallelIncrementalWMethodEQOracle<Integer> pinc = null;
        ParallelWMethodEQOracle.DFAParallelWMethodEQOracle<Integer> weq = null;


        randomWordsEQOracle = new RandomWordsEQOracle.DFARandomWordsEQOracle<>
                (statisticCacheOracle, 1, num_state, num_input * num_input, random, num_worker);

        ceq = new DFAParallelCompleteExplorationEQQracle<>(statisticCacheOracle, 1, num_state / 2, num_worker);

        increw = new IncrementalWMethodEQOracle.DFAIncrementalWMethodEQOracle<>(inputs, statisticCacheOracle);

        pinc = new ParallelIncrementalWMethodEQOracle.DFAParallelIncrementalWMethodEQOracle<>(inputs, statisticCacheOracle, 2, num_worker);

        weq = new ParallelWMethodEQOracle.DFAParallelWMethodEQOracle<>(1, statisticCacheOracle, num_worker);
//        de.learnlib.experiments.Experiment.DFAExperiment<Integer> experiment =
//                new de.learnlib.experiments.Experiment.DFAExperiment<>(tttpc, ceq, inputs);
//
//        experiment.runDFA();

        LearningAlgorithm.DFALearner<Integer> learner = tttpc;
        EquivalenceOracle.DFAEquivalenceOracle<Integer> equivalenceOracle = pinc;
        StatisticOracle.DFAStatisticOracle<Integer> statisticOracle = countCache ? statisticCacheOracle : statisticParallelOracle;

        List<Integer> roundMarker = new ArrayList<>();

        try {
            learner.startLearning();
            int round = 0;

            boolean done = false;
            DFA<?, Integer> hyp = null;
            while (!done) {
                roundMarker.add(statisticOracle.getQueryCount().size());
                hyp = learner.getHypothesisModel();

                DefaultQuery<Integer, Boolean> ce = equivalenceOracle.findCounterExample(hyp, inputs);
                if (ce == null) {
                    done = true;
                    continue;
                }

                learner.refineHypothesis(ce);
            }
        } finally {
            parallelOracle.shutdown();
        }

        //System.out.println("done searching");
        //System.out.println(roundMarker);

        Collection<List<Integer>> statistic = statisticOracle.getQueryCount();
        try {
            DFA<?, Integer> result = tttpc.getHypothesisModel();
            if (showGraph) {
                Writer w = DOT.createDotWriter(true);
                GraphDOT.write(result, inputs, w);
                w.close();
                Writer w2 = DOT.createDotWriter(true);
                GraphDOT.write(targetOriginal, inputs, w2);
                w2.close();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        if (writer != null)
            try {
                writer.format("state count = %d\n", num_state);
                writer.format("input count = %d\n", num_input);
                writer.format("worker count = %d\n", num_worker);
                writer.format("batch = %d\n", statistic.size());

                int queryCount = 0;
                int wordCount = 0;
                for (List<Integer> batch : statistic) {
                    int wordCountBatch = 0;
                    for (int word : batch) wordCountBatch += word;
                    writer.format("%d\t%d\n", batch.size(), wordCountBatch);
                    queryCount += batch.size();
                    wordCount += wordCountBatch;
                }
                writer.format("query count = %d\nword count = %d", queryCount, wordCount);
                System.out.format("query count = %d\nword count = %d\n", queryCount, wordCount);


            } finally {
                writer.close();

            }
        List<Pair<String, Integer>> source = statisticOracle.getSourceCounter();

        return new Collection[]{statistic, roundMarker, source};
    }

    public static CompactDFA<Integer> randomDFA(int num_input, int num_state, Random random) {

        Alphabet<Integer> sigma = Alphabets.integers(0, num_input - 1);
        Integer[] inputs = new Integer[num_input];
        sigma.toArray(inputs);
        CompactDFA<Integer> dfa = RandomAutomata.randomDFA(random, num_state, sigma, true);
        return dfa;
    }

    public static <I> CompactDFA<I> cloneDFA(CompactDFA<I> origin) {
        Alphabet<I> sigma = origin.getInputAlphabet();
        int num_state = origin.size();
        List<I> inputs = new ArrayList<>(origin.numInputs());
        for (Iterator<I> it = sigma.iterator(); it.hasNext(); )
            inputs.add(it.next());
        CompactDFA<I> clone = new CompactDFA<I>(sigma, num_state);
        AutomatonLowLevelCopy.copy(AutomatonCopyMethod.DFS, origin, inputs, clone);
        return clone;
    }

    public static CompactDFA<Integer> constructSumDFA(int num_state, int num_input, int accept, boolean shuffle) {
        Alphabet<Integer> sigma = Alphabets.integers(0, num_input - 1);

        CompactDFA<Integer> dfa = new CompactDFA<>(sigma);

        // create states
        int states[] = new int[num_state];
        states[0] = dfa.addInitialState(accept == 0);
        for (int i = 1; i < num_state; i++) {
            states[i] = dfa.addState(accept == i);
        }

        // create transitions
        List<Integer> inputs = new ArrayList<>();
        for (int i = 0; i < num_input; i++)
            inputs.add(i);
        if (shuffle)
            Collections.shuffle(inputs);

        for (int i = 0; i < num_state; i++)
            for (int j = 0; j < num_input; j++)
                dfa.addTransition(states[i], j, states[(i + inputs.get(j)) % num_state]);
        return dfa;
    }

    public static String linechart(Map<Integer, Double> data, String labelX, Map<String, Integer> para) {
        double scaleY = Collections.max(data.values()) + 5;
        double scaleX = Collections.max(data.keySet()) + 10;
        List<Integer> keys = new ArrayList<>();
        List<Double> keysScaled = new ArrayList<>();
        List<Double> values = new ArrayList<>();
        List<Double> sizes = new ArrayList<>();
        keys.addAll(data.keySet());
        Collections.sort(keys);
        for (int k : keys) {
            sizes.add(20.0);
            keysScaled.add((double) k * 100 / scaleX);
            values.add(data.get(k) * 100 / scaleY);
            System.out.printf("%d:%f\n", k, data.get(k));
        }

        Data d1 = Data.newData(keysScaled);
        Data d2 = Data.newData(values);
        Data dSize = Data.newData(sizes);
        ScatterPlotData scatterPlotData = Plots.newScatterPlotData(d1, d2, dSize);
        scatterPlotData.addShapeMarkers(Shape.CIRCLE, DARKGRAY, 10);
        for (int i = 0; i < values.size(); i++)
            scatterPlotData.addTextMarker(String.format("%.1f", values.get(i) * scaleY / 100), DARKSLATEGRAY, 9, i);
        ScatterPlot chart = GCharts.newScatterPlot(scatterPlotData);

        chart.setTitle("Mean Size of Batches");
        chart.setSize(1000, 300);
        chart.setBackgroundFill(Fills.newSolidFill(ALICEBLUE));
        chart.setAreaFill(Fills.newSolidFill(WHITE));

        chart.setGrid(10, 10, 5, 5);
        int labelHeight = 95;
        for (String name : para.keySet()) {
            chart.addMarker(Markers.newTextMarker(String.format("%s : %d", name, para.get(name)), BLACK, 10), 95, labelHeight);
            labelHeight -= 5;
        }

        chart.addYAxisLabels(AxisLabelsFactory.newNumericRangeAxisLabels(0, scaleY));
        chart.addXAxisLabels(AxisLabelsFactory.newNumericRangeAxisLabels(0, scaleX));

        AxisStyle axisStyle = AxisStyle.newAxisStyle(BLACK, 13, AxisTextAlignment.CENTER);
        AxisLabels yLabel = AxisLabelsFactory.newAxisLabels("Mean size of batches", 50.0);

        yLabel.setAxisStyle(axisStyle);
        AxisLabels xLabel = AxisLabelsFactory.newAxisLabels(labelX, 50.0);
        xLabel.setAxisStyle(axisStyle);
        chart.addYAxisLabels(yLabel);
        chart.addXAxisLabels(xLabel);
        return chart.toURLString();
    }

*/
/*    public static CompactDFA<Integer> constructRandomDFA(int numState, int numInput, Random random) {
        Alphabet<Integer> sigma = Alphabets.integers(0, numInput - 1);
        CompactDFA<Integer> dfa = new CompactDFA<Integer>(sigma);

        int states[] = new int[numState];

        //ensure the dfa contains at least one accepting state and one not
        boolean accepting = random.nextBoolean();
        boolean acceptingExists = accepting;
        boolean rejectingExists = !accepting;
        states[0] = dfa.addInitialState(random.nextBoolean());
        for (int i = 1; i < numState; i++) {
            if (i == numState - 1 && (!acceptingExists || !rejectingExists))
                dfa.addState(rejectingExists);
            else {
                accepting = random.nextBoolean();
                acceptingExists = accepting == true;
                rejectingExists = accepting == false;
                states[i] = dfa.addState(accepting);
            }
        }

        boolean transitionsOccupied[][] = new boolean[numState][numInput];
        for (int i = 0; i < numState; i++)
            for (int j = 0; j < numInput; j++)
                transitionsOccupied[i][j] = false;

        // ensure every state is reachable
        List<Integer> reached = new ArrayList<>();
        List<Integer> unreachedPreShuffle = new ArrayList<>();
        reached.add(states[0]);
        for (int i = 1; i < numState; i++)
            unreachedPreShuffle.add(states[i]);
        Collections.shuffle(unreachedPreShuffle);
        Deque<Integer> unreached = new LinkedList<>(unreachedPreShuffle);
        while (unreached.size() > 0) {
            Collections.shuffle(reached);
            int start = reached.get(0);
            int destiny = unreached.pop();
            boolean successful = false;
            int roundCountdown = numInput;
            while (roundCountdown-- > 0 && !successful) {
                int input = random.nextInt(numInput);
                if (!transitionsOccupied[start][input]) {
                    successful = true;
                    reached.add(destiny);
                    transitionsOccupied[start][input] = true;
                    dfa.addTransition(start, input, destiny);
                }
            }
            if (!successful) { // choose another start
                unreached.addLast(destiny);
            }
        }
        for (int i = 0; i < numState; i++)
            for (int j = 0; j < numInput; j++)
                if (!transitionsOccupied[i][j])
                    dfa.addTransition(i, j, random.nextInt(numState));

        return dfa;
    }*//*


*/
/*    public static <I> CompactDFA<I> cloneDFA(CompactDFA<I> original) {
        Alphabet<I> inputs = original.getInputAlphabet();
        CompactDFA<I> newDFA = new CompactDFA<I>(inputs);
        Collection<Integer> states = original.getStates();
        int initState = original.getInitialState();
        for (int i : states) {
            boolean acc = original.isAccepting(i);
            if (i == initState)
                newDFA.addInitialState(acc);
            else
                newDFA.addState(acc);
        }
        for (int i : states) {
            for (I j : inputs) {
                newDFA.addTransition(i, j, original.getTransition(i, j));
            }
        }
        return newDFA;
    }*//*


    public static String barchart(Collection<List<Integer>> statistics, Collection<Integer> markers, Map<String, Integer> para) {

        // Defining data plots.
        int width = 1000;
        List<Double> counts = new ArrayList<>();
        double batchSum = 0;
        for (List<Integer> batch : statistics) {
            counts.add((double) batch.size());
            batchSum += batch.size();
        }


        double scale = Collections.max(counts) + 5;
        for (int i = 0; i < counts.size(); i++) {
            double count = counts.get(i);
            counts.set(i, count * 100 / scale);
        }
        BarChartPlot queryCount = Plots.newBarChartPlot(Data.newData(counts), SLATEGRAY, "");

        // Instantiating chart.
        BarChart chart = GCharts.newBarChart(queryCount);


        // Defining axis info and styles
        AxisStyle axisStyle = AxisStyle.newAxisStyle(BLACK, 13, AxisTextAlignment.CENTER);
        AxisLabels yLabel = AxisLabelsFactory.newAxisLabels("Size", 50.0);

        yLabel.setAxisStyle(axisStyle);
        AxisLabels xLabel = AxisLabelsFactory.newAxisLabels("Process", 50.0);
        xLabel.setAxisStyle(axisStyle);

        // Adding axis info to chart.
        chart.addXAxisLabels(AxisLabelsFactory.newNumericRangeAxisLabels(0, counts.size(), (counts.size() / 11) + 1));
        chart.addYAxisLabels(AxisLabelsFactory.newNumericRangeAxisLabels(0, scale, (int) (scale / 11) + 1));
        chart.addYAxisLabels(yLabel);
        chart.addXAxisLabels(xLabel);


        int barWidth = (int) ((double) (width - 100) / counts.size()) - 1;
        int barInterval = (int) ((double) (width - 100) / counts.size()) / 3;
        chart.setSize(width, 300);
        chart.setBarWidth(barWidth > 0 ? barWidth : 1);
        chart.setSpaceWithinGroupsOfBars(1);
        chart.setDataStacked(true);
        chart.setTitle("Size of Batches", BLACK, 16);
        chart.setGrid(100, 10, 3, 2);
        chart.setBackgroundFill(Fills.newSolidFill(ALICEBLUE));
        // LinearGradientFill fill = Fills.newLinearGradientFill(0, LAVENDER, 100);
        chart.setAreaFill(Fills.newSolidFill(WHITE));
        // fill.addColorAndOffset(WHITE, 0);
        // chart.setAreaFill(fill);
        Color[] colors = new Color[]{GAINSBORO, SILVER};
        double last = 0;
        double gap = 0.003;
        StringBuffer sb = new StringBuffer();
        sb.append("&chm=@t");
        for (String name : para.keySet()) {
            sb.append(name).append(": ").append(para.get(name));
            sb.append(" ");
        }
        sb.append(",000000,0,0.9:0.95,10,1");
        sb.append("|@tBatch Count: ").append(counts.size()).append(",000000,0,0.9:0.90,10,1");
        sb.append("|@tMean Batch Size: ").append(String.format("%.2f", batchSum / counts.size())).append(",000000,0,0.9:0.85,10,1");

        for (Iterator<Integer> it = markers.iterator(); it.hasNext(); ) {
            int marker = it.next();
            //chart.addMarker(Markers.newShapeMarker(Shape.VERTICAL_LINE_FULL,BLACK,2,Priority.HIGH),(double)marker/counts.size(),100);
            double period = (double) (marker) / (counts.size());
            //period=period*(1-3*gap)+2*gap;
            sb.append("|R,").append(GRAY).append(",0,").append(period + 0.001).append(",").append(period - 0.001);
            last = period;
        }

        if (counts.size() < 80)
            sb.append("|N,000000,0,-1,11&chds=0,").append(scale);
        //chart.addMarker(Markers.newTextMarker("Batch Count: "+counts.size(), BLACK, 12, Priority.HIGH), 90, 90);
        String url = chart.toURLString() + sb.toString();
        return url;
    }

    static void showChart(String urlStr) throws IOException, URISyntaxException {
        URL url = new URL(urlStr);
        URI uri = new URI(url.getProtocol(), url.getUserInfo(), url.getHost(), url.getPort(), url.getPath(), url.getQuery(), url.getRef());
        //String encodedURL = URLEncoder.encode(url.replace("http://",""), "UTF-8");
        if (Desktop.isDesktopSupported()) {
            Desktop.getDesktop().browse(uri);
        }
    }

    static void writeHTML(PrintWriter writer, Map<String, String> urls) {
        StringBuilder sb = new StringBuilder();
        sb.append("<!DOCTYPE html>");
        sb.append("<head><title>Output</title></head>");
        sb.append("<body>");
        sb.append("<h1>Parallel TTT Output</h1>");
        for (String name : urls.keySet()) {
            sb.append("<h4>").append(name).append("</h4>");
            sb.append("<p><img src='").append(urls.get(name)).append("'/></p>");
        }
        sb.append("</body>");
        sb.append("</html>");
        String html = sb.toString();
        System.out.println(html);
        writer.println(html);
        writer.close();
    }

    static void traceError() throws IOException {
        int num_worker = 4;

        Alphabet<Integer> inputs = Alphabets.integers(0, 1);
        CompactDFA<Integer> targetOriginal = new CompactDFA<Integer>(inputs);
        targetOriginal.addInitialState(true);
        targetOriginal.addState(true);
        targetOriginal.addState(false);
        targetOriginal.addState(true);
        targetOriginal.addTransition(0, 0, 2);
        targetOriginal.addTransition(0, 1, 0);
        targetOriginal.addTransition(1, 0, 3);
        targetOriginal.addTransition(1, 1, 2);
        targetOriginal.addTransition(2, 0, 1);
        targetOriginal.addTransition(2, 1, 3);
        targetOriginal.addTransition(3, 0, 0);
        targetOriginal.addTransition(3, 1, 2);

        Collection<MembershipOracle.DFAMembershipOracle<Integer>> oracles = new ArrayList<>();
        for (int i = 0; i < num_worker; i++) {
            CompactDFA<Integer> target = cloneDFA(targetOriginal);
            MembershipOracle.DFAMembershipOracle<Integer> sul = new SimulatorOracle.DFASimulatorOracle<>(target);
            oracles.add(sul);
        }
        StatisticStaticParallelOracle<Integer, Boolean> parallelOracle =
                new StatisticStaticParallelOracle<>(oracles, 1, StatisticStaticParallelOracle.PoolPolicy.FIXED);


        Collection<MembershipOracle.DFAMembershipOracle<Integer>> eqOracles = new ArrayList<>();
        for (int i = 0; i < num_worker; i++) {
            CompactDFA<Integer> target = cloneDFA(targetOriginal);
            MembershipOracle.DFAMembershipOracle<Integer> sul = new SimulatorOracle.DFASimulatorOracle<>(target);
            eqOracles.add(sul);
        }
        MembershipOracle<Integer, Boolean> eqParallelOracles = null;
        MembershipOracle.DFAMembershipOracle<Integer> eqSUL = null;

        eqParallelOracles = new StatisticStaticParallelOracle<>(oracles, 1, StatisticStaticParallelOracle.PoolPolicy.CACHED);
        eqSUL = DFACaches.createCache(inputs, eqParallelOracles);

        DFACacheOracle<Integer> cacheOracle = DFACaches.createCache(inputs, parallelOracle);
        MembershipOracle.DFAMembershipOracle<Integer> traceO = new StatisticOracle.DFAStatisticOracle<Integer>(cacheOracle, true);

        TTTParallelLearnerOldDFA tttpc = new TTTParallelLearnerDFABuilder<Integer>()
                .withAlphabet(inputs)
                .withOracle(traceO)
                .withBatchSize(num_worker)
                .create();

        TTTLearnerDFA<Integer> ttt = new TTTLearnerDFABuilder<Integer>()
                .withAlphabet(inputs)
                .withOracle(traceO)
                .create();

        IncrementalWMethodEQOracle.DFAEquivalenceOracle<Integer> increw =
                new ParallelIncrementalWMethodEQOracle.DFAParallelIncrementalWMethodEQOracle<Integer>(inputs, traceO, 1, 4);

        tttpc.startLearning();
        boolean done = false;
        DFA<?, Integer> hyp = null;
        while (!done) {
            //System.out.format("Round: %d\n", round++);

            hyp = tttpc.getHypothesisModel();

            DefaultQuery ce = increw.findCounterExample(hyp, inputs);
            if (ce == null) {
                done = true;
                continue;
            }

            tttpc.refineHypothesis(ce);
        }
        parallelOracle.shutdown();
        ((StatisticStaticParallelOracle<Integer, Boolean>) eqParallelOracles).shutdown();
    }

    public static class ExperimentSettings {
        private final int numInput;
        private final int numState;
        private final int numOutput;
        private final int numWorker;
        private final boolean countEQ;
        private final boolean useCache;
        private final AutomataType automataType;
        private final Random random;

        public ExperimentSettings(int numInput, int numState, int num_output, int num_worker, boolean countEQ, boolean useCache, AutomataType automataType, Random random) {
            this.numInput = numInput;
            this.numState = numState;
            this.numOutput = num_output;
            this.numWorker = num_worker;
            this.countEQ = countEQ;
            this.useCache = useCache;
            this.automataType = automataType;
            this.random = random;
        }

        public AutomataType getAutomataType() {
            return automataType;
        }

        public Random getRandom() {
            return random;
        }

        public int getNumInput() {
            return numInput;
        }

        public int getNumState() {
            return numState;
        }

        public int getNumOutput() {
            return numOutput;
        }

        public int getNumWorker() {
            return numWorker;
        }

        public boolean isCountEQ() {
            return countEQ;
        }

        public boolean isUseCache() {
            return useCache;
        }

        public static enum AutomataType {
            DFA,
            MEALY
        }
    }

}
*/
