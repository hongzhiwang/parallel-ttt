package de.learnlib.algorithms.ttt.experiments.tools;

import net.automatalib.automata.fsa.DFA;
import net.automatalib.automata.fsa.impl.compact.CompactDFA;
import net.automatalib.automata.transout.MealyMachine;
import net.automatalib.automata.transout.impl.compact.CompactMealy;
import net.automatalib.util.automata.copy.AutomatonCopyMethod;
import net.automatalib.util.automata.copy.AutomatonLowLevelCopy;
import net.automatalib.util.automata.random.RandomAutomata;
import net.automatalib.words.Alphabet;

import java.util.List;
import java.util.Random;

public class AutomataHelper {
    public static <I> DFA<?, I> cloneDFA(DFA<?, I> target, Alphabet<I> alphabet) {
        CompactDFA<I> clone = new CompactDFA<>(alphabet, target.size());
        AutomatonLowLevelCopy.copy(AutomatonCopyMethod.DFS, target, alphabet, clone);
        return clone;
    }

    public static <I, O> MealyMachine<?, I, ?, O> cloneMealy(MealyMachine<?, I, ?, O> target, Alphabet<I> alphabet) {
        CompactMealy<I, O> clone = new CompactMealy<I, O>(alphabet);
        AutomatonLowLevelCopy.copy(AutomatonCopyMethod.DFS, target, alphabet, clone);
        return clone;
    }

    public static <I> CompactDFA<I> randomDFA(Alphabet<I> alphabet, int numStates, Random random) {
        while (true) {
            CompactDFA<I> dfa = RandomAutomata.randomDFA(random, numStates, alphabet, true);
            if (dfa.size() == numStates) return dfa;
        }
    }

    public static <I, O> CompactMealy<I, O> randomMealy(Alphabet<I> alphabet, List<O> outputs, int numStates, Random random) {
        while (true) {
            CompactMealy<I, O> mealy = RandomAutomata.randomMealy(random, numStates, alphabet, outputs);
            if(mealy.size()==numStates) return mealy;
        }
    }

}
