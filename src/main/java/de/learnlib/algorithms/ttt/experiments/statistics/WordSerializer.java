package de.learnlib.algorithms.ttt.experiments.statistics;

import net.automatalib.words.Alphabet;
import net.automatalib.words.Word;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

/**
 * This class translates word to number, and backwards
 */

public class WordSerializer {

    public static <I> BigInteger wordToBigint(Alphabet<I> alphabet, Word<I> word){
        BigInteger answer=BigInteger.ZERO;
        BigInteger ten=BigInteger.valueOf(alphabet.size()+1);
        for(int i=word.size()-1;i>=0;i--){
            answer=answer.multiply(ten);
            I symbol=word.getSymbol(i);
            int symind=alphabet.getSymbolIndex(symbol);

            answer=answer.add(BigInteger.valueOf(symind + 1));
        }
        return answer;
    }

    public static <I> Word<I> BigintToWord(Alphabet<I> alphabet, BigInteger serial){
        List<I> word=new ArrayList<>();
        BigInteger ten=BigInteger.valueOf(alphabet.size()+1);
        BigInteger curr=serial;
        while(curr.compareTo(BigInteger.ZERO)>0){
            BigInteger[] results=curr.divideAndRemainder(ten);
            curr=results[0];
            BigInteger remainder=results[1];
            int sym=remainder.intValue()-1;
            assert sym>=0;
            word.add(alphabet.getSymbol(sym));
        }
        return Word.fromList(word);
    }

    public static int getLength(Alphabet alphabet, BigInteger serial){
        BigInteger ten=BigInteger.valueOf(alphabet.size()+1);
        BigInteger curr=serial;
        int length=0;
        while(curr.compareTo(BigInteger.ZERO)>0){
            BigInteger[] results=curr.divideAndRemainder(ten);
            curr=results[0];
            length++;
        }
        return length;
    }
}
