/* Copyright (C) 2013 TU Dortmund
 * This file is part of LearnLib, http://www.learnlib.de/.
 *
 * LearnLib is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License version 3.0 as published by the Free Software Foundation.
 *
 * LearnLib is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with LearnLib; if not, see
 * <http://www.gnu.de/documents/lgpl.en.html>.
 */
package de.learnlib.algorithms.ttt.experiments;

import de.learnlib.algorithms.ttt.dfa.TTTLearnerDFA;
import de.learnlib.algorithms.ttt.dfa.TTTLearnerDFABuilder;
import de.learnlib.algorithms.ttt.dfa.TTTParallelLearnerDFA;
import de.learnlib.algorithms.ttt.dfa.TTTParallelLearnerDFABuilder;
import de.learnlib.api.MembershipOracle.DFAMembershipOracle;
import de.learnlib.cache.dfa.DFACacheOracle;
import de.learnlib.cache.dfa.DFACaches;
import de.learnlib.eqtests.basic.RandomWordsEQOracle;
import de.learnlib.eqtests.basic.WMethodEQOracle.DFAWMethodEQOracle;
import de.learnlib.eqtests.basic.WpMethodEQOracle;
import de.learnlib.experiments.Experiment.DFAExperiment;
import de.learnlib.oracles.SimulatorOracle.DFASimulatorOracle;
import de.learnlib.parallelism.ParallelWMethodEQOracle;
import de.learnlib.parallelism.StatisticStaticParallelOracle;
import de.learnlib.statistics.SimpleProfiler;
import net.automatalib.automata.fsa.DFA;
import net.automatalib.automata.fsa.impl.compact.CompactDFA;
import net.automatalib.commons.dotutil.DOT;
import net.automatalib.util.graphs.dot.GraphDOT;
import net.automatalib.words.Alphabet;
import net.automatalib.words.impl.Alphabets;

import java.io.IOException;
import java.io.InputStream;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Random;

/**
 * This example shows the usage of a learning algorithm and an equivalence test
 * as part of an experiment in order to learn a simulated SUL (system under
 * learning).
 *
 * @author falkhowar
 */
public class Example {

    /**
     * creates example from Angluin's seminal paper.
     *
     * @return example dfa
     */
    private static CompactDFA<Character> constructSUL() {
        // input alphabet contains characters 'a'..'b'
        Alphabet<Character> sigma = Alphabets.characters('a', 'b');

        // create states
        CompactDFA<Character> dfa = new CompactDFA<>(sigma);
        int q0 = dfa.addInitialState(false);
        int q1 = dfa.addState(false);
        int q2 = dfa.addState(false);
        int q3 = dfa.addState(false);
        int q4 = dfa.addState(false);
        int q5 = dfa.addState(true);


        // create transitions
        dfa.addTransition(q0, 'a', q1);
        dfa.addTransition(q0, 'b', q3);
        dfa.addTransition(q1, 'a', q2);
        dfa.addTransition(q1, 'b', q4);
        dfa.addTransition(q2, 'a', q2);
        dfa.addTransition(q2, 'b', q5);
        dfa.addTransition(q3, 'a', q4);
        dfa.addTransition(q3, 'b', q0);
        dfa.addTransition(q4, 'a', q5);
        dfa.addTransition(q4, 'b', q1);
        dfa.addTransition(q5, 'a', q5);
        dfa.addTransition(q5, 'b', q2);

        return dfa;
    }

    public static CompactDFA<Integer> constructSumDFA(int max, int accept){
        Alphabet<Integer> sigma = Alphabets.integers(0,max);
        CompactDFA<Integer> dfa = new CompactDFA<>(sigma);

        // create states
        int states[]=new int[max+1];
        states[0]=dfa.addInitialState(accept==0);
        for(int i=1;i<=max;i++){
            states[i]=dfa.addState(accept==i);
        }
        for(int i=0;i<=max;i++)
            for(int j=0;j<=max;j++)
                dfa.addTransition(states[i],j,states[(i+j)%(max+1)]);
        return dfa;
    }
    public static void main(String[] args) throws IOException {

        // load DFA and alphabet
        //CompactDFA<Character> target = constructSUL();
        InputStream is=Example.class.getResourceAsStream("/pots2.dfa.gz");
        //CompactDFA<Integer> target=AUTImporter.read(is);
        CompactDFA<Integer> target = constructSumDFA(19, 3);
        Alphabet<Integer> inputs = target.getInputAlphabet();

        // construct a simulator membership query oracle
        // input  - Character (determined by example)
        DFAMembershipOracle<Integer> sul = new DFASimulatorOracle<>(target);

        // oracle for counting queries wraps SUL
        //DFAStatisticOracle<Character> mqOracle =
        //        new DFAStatisticOracle<>(sul, "membership queries");

        int num_workers=5;
        Collection<DFAMembershipOracle<Integer>> oracles=new ArrayList<>();
        for(int i=0;i<num_workers;i++)
            oracles.add(sul);

        StatisticStaticParallelOracle<Integer,Boolean> parallelOracle=
                new StatisticStaticParallelOracle<>(oracles,1, StatisticStaticParallelOracle.PoolPolicy.CACHED);

        //DFAMembershipOracle<Character> sulp=new StatisticStaticParallelOracle<>(target,8, ParallelOracle.PoolPolicy.CACHED);


        DFACacheOracle<Integer> cacheOracle= DFACaches.createCache(inputs, parallelOracle);


        // construct L* instance
//        ExtensibleLStarDFA<Integer> lstar = new ExtensibleLStarDFABuilder<Integer>()
//                .withAlphabet(inputs) // input alphabet
//                .withOracle(cacheOracle) // membership oracle
//                .create();

        TTTLearnerDFA<Integer> ttt=new TTTLearnerDFABuilder<Integer>()
                .withAlphabet(inputs)
                .withOracle(cacheOracle)
                .create();

        TTTParallelLearnerDFA<Integer> tttp=new TTTParallelLearnerDFABuilder<Integer>()
                .withAlphabet(inputs)
                .withOracle(cacheOracle)
                .create();

        TTTParallelLearnerDFA tttpc=new TTTParallelLearnerDFABuilder<Integer>()
                .withAlphabet(inputs)
                .withOracle(cacheOracle)
                .create();

        TTTLearnerDFA tttc=new TTTLearnerDFABuilder<Integer>()
                .withAlphabet(inputs)
                .withOracle(cacheOracle)
                .create();

        // construct a W-method conformance test
        // exploring the system up to depth 4 from
        // every state of a hypothesis
        ParallelWMethodEQOracle.DFAParallelWMethodEQOracle<Integer> parallelWMethod =
                new ParallelWMethodEQOracle.DFAParallelWMethodEQOracle<>(100, sul,num_workers);

        DFAWMethodEQOracle<Integer> wMethod=new DFAWMethodEQOracle<>(4,parallelOracle);
        WpMethodEQOracle.DFAWpMethodEQOracle<Integer> wpMethodEQOracle=new WpMethodEQOracle.DFAWpMethodEQOracle<>(4,parallelOracle);

        RandomWordsEQOracle.DFARandomWordsEQOracle<Integer> randomWordsEQOracle=
                new RandomWordsEQOracle.DFARandomWordsEQOracle<Integer>(cacheOracle,1,10,100,new Random(),num_workers);

        // construct a learning experiment from
        // the learning algorithm and the conformance test.
        // The experiment will execute the main loop of
        // active learning
        DFAExperiment<Integer> experiment =
                new DFAExperiment<>(tttpc, randomWordsEQOracle, inputs);

        // turn on time profiling
        experiment.setProfile(true);

        // enable logging of models
        experiment.setLogModels(true);


        // runDFA experiment
        experiment.run();

        // get learned model
        DFA<?, Integer> result = experiment.getFinalHypothesis();

        // report results
        System.out.println("-------------------------------------------------------");

        // profiling
        System.out.println(SimpleProfiler.getResults());

        // learning statistics
        List<List<Integer>> queryCount=parallelOracle.getQueryCount();
        System.out.println("---- Statistics -----");
        for(int batch=0; batch<queryCount.size();batch++){
            System.out.format("batch %d\n",batch);
            List<Integer> batchCount=queryCount.get(batch);
            int lengthCount=0;
            for(int l: batchCount) lengthCount+=l;
            System.out.format("Queries: %d; Word count: %d\n",batchCount.size(),lengthCount);
        }
/*        for(DFAStatisticOracle<Integer>oracle:oracles){
            System.out.format("Oracle %s\n",oracle.toString());
            Map<Long,List<Integer>> count=oracle.getTimedCounter();
            for(long timestamp:count.keySet()){
                List<Integer> wordLength=count.get(timestamp);
                System.out.print("Execute [");
                for(int l: wordLength){
                    System.out.format("%d ",l);
                }
                System.out.format("] at %d\n", timestamp);
            }
        }*/

        // model statistics
        System.out.println("States: " + result.size());
        System.out.println("Sigma: " + inputs.size());

        // show model
        System.out.println();
        System.out.println("Model: ");
        GraphDOT.write(result, inputs, System.out); // may throw IOException!

        Writer w = DOT.createDotWriter(true);
        GraphDOT.write(result, inputs, w);
        w.close();

        System.out.println("-------------------------------------------------------");
//
//        System.out.println("Final observation table:");
//        new ObservationTableASCIIWriter<>().write(ttt.getObservationTable(), System.out);
//
//        OTUtils.displayHTMLInBrowser(ttt.getObservationTable());
    }
}
