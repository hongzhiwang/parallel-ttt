package de.learnlib.algorithms.ttt.experiments;

import de.learnlib.algorithms.ttt.dfa.TTTParallelLearnerDFA;
import de.learnlib.algorithms.ttt.dfa.TTTParallelLearnerDFABuilder;
import de.learnlib.algorithms.ttt.experiments.statistics.ForkOracle;
import de.learnlib.algorithms.ttt.experiments.statistics.QueryCounter;
import de.learnlib.algorithms.ttt.experiments.statistics.StatisticOracle;
import de.learnlib.algorithms.ttt.mealy.TTTParallelLearnerMealy;
import de.learnlib.algorithms.ttt.mealy.TTTParallelLearnerMealyBuilder;
import de.learnlib.api.MembershipOracle;
import de.learnlib.cache.dfa.DFACacheOracle;
import de.learnlib.cache.dfa.DFACaches;
import de.learnlib.cache.mealy.MealyCacheOracle;
import de.learnlib.cache.mealy.MealyCaches;
import de.learnlib.oracles.DefaultQuery;
import de.learnlib.oracles.SimulatorOracle;
import de.learnlib.parallelism.ParallelIncrementalWMethodEQOracle;
import de.learnlib.parallelism.ParallelOracle;
import de.learnlib.parallelism.StaticParallelOracle;
import net.automatalib.automata.fsa.DFA;
import net.automatalib.automata.fsa.impl.compact.CompactDFA;
import net.automatalib.automata.transout.MealyMachine;
import net.automatalib.automata.transout.impl.compact.CompactMealy;
import net.automatalib.util.automata.copy.AutomatonCopyMethod;
import net.automatalib.util.automata.copy.AutomatonLowLevelCopy;
import net.automatalib.util.automata.random.RandomAutomata;
import net.automatalib.words.Alphabet;
import net.automatalib.words.Word;
import net.automatalib.words.impl.Alphabets;

import java.awt.*;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.*;
import java.util.List;


public class ExperimentRunner {
    public static CompactDFA<Integer> randomDFA(int numInput, int numState, Random random) {
        while (true) {
            Alphabet<Integer> sigma = Alphabets.integers(0, numInput - 1);
            Integer[] inputs = new Integer[numInput];
            sigma.toArray(inputs);
            CompactDFA<Integer> dfa = RandomAutomata.randomDFA(random, numState, sigma, true);
            if (dfa.getStates().size() == numState)
                return dfa;
        }
    }

    public static CompactMealy<Integer, Character> randomMealy(int numInput, int numOutput, int numState, Random random) {
        while (true) {
            Alphabet<Integer> sigma = Alphabets.integers(0, numInput - 1);
            Integer[] inputs = new Integer[numInput];
            sigma.toArray(inputs);

            Alphabet<Character> lambda = Alphabets.characters('a', Character.toChars(('a' + numOutput) - 1)[0]);

            Character[] outputs = new Character[numOutput];
            lambda.toArray(outputs);

            CompactMealy<Integer, Character> mealy = RandomAutomata.randomMealy(random, numState, sigma, Arrays.asList(outputs), true);
            if (mealy.size() == numState)
                return mealy;
        }
    }

    public static <I> CompactDFA<I> cloneDFA(CompactDFA<I> origin) {
        Alphabet<I> sigma = origin.getInputAlphabet();
        CompactDFA<I> clone = new CompactDFA<>(sigma, origin.size());
        AutomatonLowLevelCopy.copy(AutomatonCopyMethod.DFS, origin, sigma, clone);
        return clone;
    }

    public static <I, O> CompactMealy<I, O> cloneMealy(CompactMealy<I, O> origin) {
        Alphabet<I> sigma = origin.getInputAlphabet();
        CompactMealy<I, O> clone = new CompactMealy<>(sigma, origin.size());
        AutomatonLowLevelCopy.copy(AutomatonCopyMethod.DFS, origin, sigma, clone);
        return clone;
    }

    public static List[] runMealy(ExperimentSetting setting) {
        CompactMealy<Integer, Character> targetOriginal = randomMealy(setting.numInput, setting.numOutput, setting.numState, setting.random);
        List<StatisticOracle.MealyStatisticOracle<Integer, Character>> oracles = new ArrayList<>();
        List<StatisticOracle.MealyStatisticOracle<Integer, Character>> shadowOracles = new ArrayList<>();


        for (int i = 0; i < setting.numWorker; i++) {
            CompactMealy<Integer, Character> target = cloneMealy(targetOriginal);
            MembershipOracle.MealyMembershipOracle<Integer, Character> sul = new SimulatorOracle.MealySimulatorOracle<>(target);
            oracles.add(new StatisticOracle.MealyStatisticOracle<>(sul, targetOriginal.getInputAlphabet()));
            shadowOracles.add(new StatisticOracle.MealyStatisticOracle<>(sul, targetOriginal.getInputAlphabet()));
        }

        ParallelOracle<Integer, Word<Character>> parallelOracle = new StaticParallelOracle<>(oracles, 1, ParallelOracle.PoolPolicy.FIXED);
        ParallelOracle<Integer, Word<Character>> shadowParallelOracle = new StaticParallelOracle<>(shadowOracles, 1, ParallelOracle.PoolPolicy.FIXED);
        StatisticOracle.MealyStatisticOracle<Integer, Character> statisticParallelOracle = new StatisticOracle.MealyStatisticOracle<>(parallelOracle, targetOriginal.getInputAlphabet());

        MealyCacheOracle<Integer, Character> cacheOracle = MealyCaches.createTreeCache(targetOriginal.getInputAlphabet(), statisticParallelOracle);

        StatisticOracle.MealyStatisticOracle<Integer, Character> statisticCacheOracle
                = new StatisticOracle.MealyStatisticOracle<>(cacheOracle, targetOriginal.getInputAlphabet());

        List<MembershipOracle<Integer, Word<Character>>> forkList = new ArrayList<>();
        forkList.add(shadowParallelOracle);
        forkList.add(statisticCacheOracle);
        ForkOracle<Integer, Word<Character>> forkOracle = new ForkOracle<>(forkList);

        TTTParallelLearnerMealy learner = new TTTParallelLearnerMealyBuilder<Integer, Character>()
                .withAlphabet(targetOriginal.getInputAlphabet())
                .withOracle(forkOracle)
                .create();

        ParallelIncrementalWMethodEQOracle.MealyParallelIncrementalWMethodEQOracle<Integer, Character> equivalenceOracle;
        equivalenceOracle = new ParallelIncrementalWMethodEQOracle.MealyParallelIncrementalWMethodEQOracle<>
                (targetOriginal.getInputAlphabet(), forkOracle, 1, setting.numWorker * 2);

        List<Integer> roundMarkerCache = new ArrayList<>();
        List<Integer> roundMarkerParallel = new ArrayList<>();

        try {
            learner.startLearning();
            boolean done = false;
            MealyMachine hyp;
            while (!done) {
                roundMarkerCache.add(statisticCacheOracle.getCounter().size());
                roundMarkerParallel.add(statisticParallelOracle.getCounter().size());
                hyp = learner.getHypothesisModel();

                DefaultQuery<Integer, Word<Character>> ce = equivalenceOracle.findCounterExample(hyp, targetOriginal.getInputAlphabet());
                if (ce == null) {
                    done = true;
                    continue;
                }

                learner.refineHypothesis(ce);
            }
        } finally {
            parallelOracle.shutdown();
            shadowParallelOracle.shutdown();
        }

        // Output

        QueryCounter cacheCounter = statisticCacheOracle.getCounter();
        QueryCounter parallelCounter = statisticParallelOracle.getCounter();
        List<QueryCounter> workerCounters = new ArrayList<>();
        for (StatisticOracle oracle : oracles)
            workerCounters.add(oracle.getCounter());
        List<QueryCounter> shadowCounters = new ArrayList<>();
        for (StatisticOracle oracle : shadowOracles)
            shadowCounters.add(oracle.getCounter());
        QueryCounter parallelWordCount = QueryCounter.parallelWordCount(parallelCounter, workerCounters, targetOriginal.getInputAlphabet());
        QueryCounter cacheWordCount = QueryCounter.parallelWordCount(cacheCounter, shadowCounters, targetOriginal.getInputAlphabet());
        return new List[]{cacheCounter, parallelCounter, cacheWordCount, parallelWordCount, roundMarkerCache, roundMarkerParallel};
    }

    public static List[] runDFA(ExperimentSetting setting) {

        // Generate Target DFA
        CompactDFA<Integer> targetOriginal = randomDFA(setting.numInput, setting.numState, setting.random);


        List<StatisticOracle.DFAStatisticOracle<Integer>> oracles = new ArrayList<>();
        List<StatisticOracle.DFAStatisticOracle<Integer>> shadowOracles = new ArrayList<>();

        for (int i = 0; i < setting.numWorker; i++) {
            CompactDFA<Integer> target = cloneDFA(targetOriginal);
            MembershipOracle.DFAMembershipOracle<Integer> sul = new SimulatorOracle.DFASimulatorOracle<>(target);
            oracles.add(new StatisticOracle.DFAStatisticOracle<>(sul, targetOriginal.getInputAlphabet()));
            shadowOracles.add(new StatisticOracle.DFAStatisticOracle<>(sul, targetOriginal.getInputAlphabet()));
        }

        ParallelOracle<Integer, Boolean> parallelOracle = new StaticParallelOracle<>(oracles, 1, ParallelOracle.PoolPolicy.FIXED);
        ParallelOracle<Integer, Boolean> shadowParallelOracle = new StaticParallelOracle<>(shadowOracles, 1, ParallelOracle.PoolPolicy.FIXED);
        StatisticOracle.DFAStatisticOracle<Integer> statisticParallelOracle = new StatisticOracle.DFAStatisticOracle<>(parallelOracle, targetOriginal.getInputAlphabet());

        DFACacheOracle<Integer> cacheOracle = DFACaches.createTreeCache(targetOriginal.getInputAlphabet(), statisticParallelOracle);

        StatisticOracle.DFAStatisticOracle<Integer> statisticCacheOracle
                = new StatisticOracle.DFAStatisticOracle<>(cacheOracle, targetOriginal.getInputAlphabet());

        List<MembershipOracle<Integer, Boolean>> forkList = new ArrayList<>();
        forkList.add(shadowParallelOracle);
        forkList.add(statisticCacheOracle);
        ForkOracle<Integer, Boolean> forkOracle = new ForkOracle<>(forkList);

        TTTParallelLearnerDFA learner = new TTTParallelLearnerDFABuilder<Integer>()
                .withAlphabet(targetOriginal.getInputAlphabet())
                .withOracle(forkOracle)
                .create();

        ParallelIncrementalWMethodEQOracle.DFAParallelIncrementalWMethodEQOracle<Integer> equivalenceOracle;
        equivalenceOracle = new ParallelIncrementalWMethodEQOracle.DFAParallelIncrementalWMethodEQOracle<>
                (targetOriginal.getInputAlphabet(), forkOracle, 1, setting.numWorker * 2);

        List<Integer> roundMarkerCache = new ArrayList<>();
        List<Integer> roundMarkerParallel = new ArrayList<>();

        try {
            learner.startLearning();
            boolean done = false;
            DFA hyp;
            while (!done) {
                roundMarkerCache.add(statisticCacheOracle.getCounter().size());
                roundMarkerParallel.add(statisticParallelOracle.getCounter().size());
                hyp = learner.getHypothesisModel();

                DefaultQuery<Integer, Boolean> ce = equivalenceOracle.findCounterExample(hyp, targetOriginal.getInputAlphabet());
                if (ce == null) {
                    done = true;
                    continue;
                }

                learner.refineHypothesis(ce);
            }

        } finally {
            parallelOracle.shutdown();
            shadowParallelOracle.shutdown();
        }

        // Output

        QueryCounter cacheCounter = statisticCacheOracle.getCounter();
        QueryCounter parallelCounter = statisticParallelOracle.getCounter();
        List<QueryCounter> workerCounters = new ArrayList<>();
        for (StatisticOracle oracle : oracles)
            workerCounters.add(oracle.getCounter());
        List<QueryCounter> shadowCounters = new ArrayList<>();
        for (StatisticOracle oracle : shadowOracles)
            shadowCounters.add(oracle.getCounter());
        QueryCounter parallelWordCount = QueryCounter.parallelWordCount(parallelCounter, workerCounters, targetOriginal.getInputAlphabet());
        QueryCounter cacheWordCount = QueryCounter.parallelWordCount(cacheCounter, shadowCounters, targetOriginal.getInputAlphabet());
        return new List[]{cacheCounter, parallelCounter, cacheWordCount, parallelWordCount, roundMarkerCache, roundMarkerParallel};
    }

    public static Map<Integer, Double> meanBatchSizeState(ExperimentSetting setting, int repeating) {
        int numStates = 1;
        Map<Integer, Double> meanBatchSize = new LinkedHashMap<>();
        for (int i = 0; i < 8; i++) {
            int batchCount = 0;
            int querySum = 0;
            numStates *= 2;
            for (int j = 0; j < repeating; j++) {
                List[] results = runMealy(new ExperimentSetting(setting.numWorker, setting.numInput, setting.numOutput, numStates, setting.random));

            }
        }
        return meanBatchSize;
    }

    static void showPage(String urlStr) throws IOException, URISyntaxException {
        URL url = new URL(urlStr);
        URI uri = new URI(url.getProtocol(), url.getUserInfo(), url.getHost(), url.getPort(), url.getPath(), url.getQuery(), url.getRef());
        //String encodedURL = URLEncoder.encode(url.replace("http://",""), "UTF-8");
        if (Desktop.isDesktopSupported()) {
            Desktop.getDesktop().browse(uri);
        }
    }

    public static class ExperimentSetting {
        final int numWorker;
        final int numInput, numOutput, numState;
        final Random random;

        public ExperimentSetting(int numWorker, int numInput, int numOutput, int numState, Random random) {
            this.numWorker = numWorker;
            this.numInput = numInput;
            this.numOutput = numOutput;
            this.numState = numState;
            this.random = random;
        }
    }

}
