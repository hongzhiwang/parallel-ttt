/* Copyright (C) 2014 TU Dortmund
 * This file is part of LearnLib, http://www.learnlib.de/.
 *
 * LearnLib is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License version 3.0 as published by the Free Software Foundation.
 *
 * LearnLib is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with LearnLib; if not, see
 * <http://www.gnu.de/documents/lgpl.en.html>.
 */
package de.learnlib.algorithms.ttt.mealy;

import com.github.misberner.buildergen.annotations.GenerateBuilder;
import de.learnlib.algorithms.ttt.base.BaseTTTParallelLearner;
import de.learnlib.algorithms.ttt.base.DTNode;
import de.learnlib.algorithms.ttt.base.TTTState;
import de.learnlib.algorithms.ttt.base.TTTTransition;
import de.learnlib.api.LearningAlgorithm;
import de.learnlib.api.MembershipOracle;
import de.learnlib.counterexamples.LocalSuffixFinder;
import de.learnlib.oracles.DefaultQuery;
import net.automatalib.automata.transout.MealyMachine;
import net.automatalib.words.Alphabet;
import net.automatalib.words.Word;
import net.automatalib.words.WordBuilder;

import java.util.*;

public class TTTParallelLearnerMealy<I, O> extends
		BaseTTTParallelLearner<MealyMachine<?, I, ?, O>, I, Word<O>> implements LearningAlgorithm.MealyLearner<I,O>{

	@GenerateBuilder(defaults = BaseTTTParallelLearner.BuilderDefaults.class)
	public TTTParallelLearnerMealy(Alphabet<I> alphabet,
								   MembershipOracle<I, Word<O>> oracle,
								   LocalSuffixFinder<? super I, ? super Word<O>> suffixFinder, boolean collectSifts, int siftDepth) {
		super(alphabet, oracle, new TTTHypothesisMealy<I,O>(alphabet), suffixFinder, collectSifts, siftDepth);
	}

	@Override
	@SuppressWarnings("unchecked")
	public MealyMachine<?, I, ?, O> getHypothesisModel() {
		return (TTTHypothesisMealy<I, O>) hypothesis;
	}


    @Override
    protected List<TTTTransition<I,Word<O>>> createTransitions(TTTState<I,Word<O>> state, Collection<I> syms){
        List<TTTTransition<I,Word<O>>> transitionList=new ArrayList<>();
        Map<I,DefaultQuery<I,Word<O>>> queryMap=new HashMap<>();
        Map<I,O> symMap=new HashMap<>();
        for(I sym:syms){
            DefaultQuery<I,Word<O>> query=new DefaultQuery<>(state.getAccessSequence(),Word.fromSymbols(sym));
            queryMap.put(sym,query);
        }
        oracle.processQueries(queryMap.values());
        for(I sym:queryMap.keySet()) {
            TTTTransitionMealy<I,O> trans = new TTTTransitionMealy<>(state, sym);
            trans.output = queryMap.get(sym).getOutput().firstSymbol();
            transitionList.add(trans);
        }
        return transitionList;
    }
/*
	@Override
	protected TTTTransition<I,Word<O>> createTransition(TTTState<I,Word<O>> state, I sym) {
		TTTTransitionMealy<I,O> trans = new TTTTransitionMealy<>(state, sym);
		trans.output = query(state, Word.fromLetter(sym)).firstSymbol();
		return trans;
	}*/

	@Override
	protected Word<O> predictSuccOutcome(TTTTransition<I, Word<O>> trans,
			DTNode<I, Word<O>> succSeparator) {
		TTTTransitionMealy<I, O> mtrans = (TTTTransitionMealy<I, O>) trans;
		if (succSeparator == null) {
			return Word.fromLetter(mtrans.output);
		}
		return succSeparator.subtreeLabel(trans.getDTTarget()).prepend(mtrans.output);
	}

	@Override
	protected Word<O> computeHypothesisOutput(TTTState<I, Word<O>> state,
			Iterable<? extends I> suffix) {
		TTTState<I,Word<O>> curr = state;
		
		WordBuilder<O> wb = new WordBuilder<>();
		
		for (I sym : suffix) {
			TTTTransitionMealy<I, O> trans = (TTTTransitionMealy<I,O>) hypothesis.getInternalTransition(curr, sym);
			wb.append(trans.output);
			curr = getTarget(trans);
		}
		
		return wb.toWord();
	}

}
