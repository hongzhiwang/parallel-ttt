/* Copyright (C) 2014 TU Dortmund
 * This file is part of LearnLib, http://www.learnlib.de/.
 * 
 * LearnLib is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License version 3.0 as published by the Free Software Foundation.
 * 
 * LearnLib is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with LearnLib; if not, see
 * <http://www.gnu.de/documents/lgpl.en.html>.
 */
package de.learnlib.parallelism;

import de.learnlib.api.EquivalenceOracle;
import de.learnlib.api.MembershipOracle;
import de.learnlib.oracles.DefaultQuery;
import net.automatalib.automata.UniversalDeterministicAutomaton;
import net.automatalib.automata.concepts.Output;
import net.automatalib.automata.fsa.DFA;
import net.automatalib.automata.transout.MealyMachine;
import net.automatalib.commons.util.collections.CollectionsUtil;
import net.automatalib.util.automata.Automata;
import net.automatalib.words.Word;
import net.automatalib.words.WordBuilder;

import java.util.*;

/**
 * Implements an equivalence test by applying the W-method test on the given
 * hypothesis automaton, as described in "Testing software design modeled by finite state machines"
 * by T.S. Chow.
 *
 * @param <A> automaton type
 * @param <I> input symbol type
 * @param <D> output domain type
 * @author Malte Isberner
 */
public class ParallelWMethodEQOracle<A extends UniversalDeterministicAutomaton<?, I, ?, ?, ?> & Output<I, D>, I, D>
        implements EquivalenceOracle<A, I, D> {

    public static class DFAParallelWMethodEQOracle<I> extends ParallelWMethodEQOracle<DFA<?, I>, I, Boolean>
            implements DFAEquivalenceOracle<I> {
        public DFAParallelWMethodEQOracle(int maxDepth,
                                          MembershipOracle<I, Boolean> sulOracle, int batchSize) {
            super(maxDepth, sulOracle, batchSize);
        }
    }

    public static class MealyParallelWMethodEQOracle<I, O> extends ParallelWMethodEQOracle<MealyMachine<?, I, ?, O>, I, Word<O>>
            implements MealyEquivalenceOracle<I, O> {
        public MealyParallelWMethodEQOracle(int maxDepth,
                                            MembershipOracle<I, Word<O>> sulOracle, int batchSize) {
            super(maxDepth, sulOracle, batchSize);
        }
    }

    private int maxDepth;
    private final MembershipOracle<I, D> sulOracle;
    private final int maxBatchSize;

    /**
     * Constructor.
     *
     * @param maxDepth  the maximum length of the "middle" part of the test cases
     * @param sulOracle interface to the system under learning
     */
    public ParallelWMethodEQOracle(int maxDepth, MembershipOracle<I, D> sulOracle, int maxBatchSize) {
        this.maxDepth = maxDepth;
        this.sulOracle = sulOracle;
        this.maxBatchSize = maxBatchSize;
    }

    public void setMaxDepth(int maxDepth) {
        this.maxDepth = maxDepth;
    }

    /*
     * (non-Javadoc)
     * @see de.learnlib.api.EquivalenceOracle#findCounterExample(java.lang.Object, java.util.Collection)
     */
    @Override
    public DefaultQuery<I, D> findCounterExample(A hypothesis,
                                                 Collection<? extends I> inputs) {

        List<Word<I>> transCover = Automata.transitionCover(hypothesis, inputs);
        List<Word<I>> charSuffixes = Automata.characterizingSet(hypothesis, inputs);

        // Special case: List of characterizing suffixes may be empty,
        // but in this case we still need to test!
        if (charSuffixes.isEmpty())
            charSuffixes = Collections.singletonList(Word.<I>epsilon());


        WordBuilder<I> wb = new WordBuilder<>();

        List<DefaultQuery<I, D>> queries = new ArrayList<>();

        for (List<? extends I> middle : CollectionsUtil.allTuples(inputs, 1, maxDepth)) {
            for (Word<I> trans : transCover) {
                for (Word<I> suffix : charSuffixes) {
                    wb.append(trans).append(middle).append(suffix);
                    Word<I> queryWord = wb.toWord();
                    wb.clear();
                    DefaultQuery<I, D> query = new DefaultQuery<>(queryWord);
                    queries.add(query);
                    if(queries.size()>= maxBatchSize){
                        sulOracle.processQueries(queries);
                        for(DefaultQuery<I,D>q:queries){
                            D hypOutput=hypothesis.computeOutput(q.getInput());
                            if (!Objects.equals(hypOutput, q.getOutput()))
                                return q;
                        }
                        queries.clear();
                    }
                }
            }
        }
        if(queries.size()>0){
            sulOracle.processQueries(queries);
            for(DefaultQuery<I,D>q:queries){
                D hypOutput=hypothesis.computeOutput(q.getInput());
                if (!Objects.equals(hypOutput, q.getOutput()))
                    return q;
            }
            queries.clear();
        }
        return null;
    }

}