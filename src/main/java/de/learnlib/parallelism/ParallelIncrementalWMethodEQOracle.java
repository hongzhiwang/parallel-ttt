/* Copyright (C) 2014 TU Dortmund
 * This file is part of LearnLib, http://www.learnlib.de/.
 * 
 * LearnLib is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License version 3.0 as published by the Free Software Foundation.
 * 
 * LearnLib is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with LearnLib; if not, see
 * <http://www.gnu.de/documents/lgpl.en.html>.
 */
package de.learnlib.parallelism;

import de.learnlib.api.EquivalenceOracle;
import de.learnlib.api.MembershipOracle;
import de.learnlib.oracles.DefaultQuery;
import net.automatalib.automata.UniversalDeterministicAutomaton;
import net.automatalib.automata.concepts.Output;
import net.automatalib.automata.fsa.DFA;
import net.automatalib.automata.transout.MealyMachine;
import net.automatalib.util.automata.conformance.IncrementalWMethodTestsIterator;
import net.automatalib.words.Alphabet;
import net.automatalib.words.Word;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Objects;

public class ParallelIncrementalWMethodEQOracle<A extends UniversalDeterministicAutomaton<?, I, ?, ?,?> & Output<I,D>, I, D>
		implements EquivalenceOracle<A, I, D> {

	public static class DFAParallelIncrementalWMethodEQOracle<I>
			extends ParallelIncrementalWMethodEQOracle<DFA<?,I>, I, Boolean>
			implements DFAEquivalenceOracle<I> {
		public DFAParallelIncrementalWMethodEQOracle(Alphabet<I> alphabet,
				MembershipOracle<I, Boolean> oracle, int maxDepth, int batchSize) {
			super(alphabet, oracle, maxDepth, batchSize);
		}
		public DFAParallelIncrementalWMethodEQOracle(Alphabet<I> alphabet,
				MembershipOracle<I, Boolean> oracle) {
			super(alphabet, oracle);
		}
	}

	public static class MealyParallelIncrementalWMethodEQOracle<I,O>
			extends ParallelIncrementalWMethodEQOracle<MealyMachine<?,I,?,O>, I, Word<O>>
			implements MealyEquivalenceOracle<I, O> {
		public MealyParallelIncrementalWMethodEQOracle(Alphabet<I> alphabet,
													   MembershipOracle<I, Word<O>> oracle, int maxDepth, int batchSize) {
			super(alphabet, oracle, maxDepth, batchSize);
		}
		public MealyParallelIncrementalWMethodEQOracle(Alphabet<I> alphabet,
													   MembershipOracle<I, Word<O>> oracle) {
			super(alphabet, oracle);
		}
	}

	@SuppressWarnings("unused")
	private final Alphabet<I> alphabet;
	private final MembershipOracle<I, D> oracle;
	private final IncrementalWMethodTestsIterator<I> incrementalWMethodIt;
	private final int batchSize;

	private int maxDepth;

	public ParallelIncrementalWMethodEQOracle(Alphabet<I> alphabet, MembershipOracle<I, D> oracle) {
		this(alphabet, oracle, 1,1);
	}

	public ParallelIncrementalWMethodEQOracle(Alphabet<I> alphabet, MembershipOracle<I, D> oracle, int maxDepth, int batchSize) {
		this.alphabet = alphabet;
		this.oracle = oracle;
		this.incrementalWMethodIt = new IncrementalWMethodTestsIterator<>(alphabet);
		this.incrementalWMethodIt.setMaxDepth(maxDepth);
		
		this.maxDepth = maxDepth;
		this.batchSize=batchSize;
	}

	public int getMaxDepth() {
		return maxDepth;
	}
	
	public void setMaxDepth(int maxDepth) {
		this.maxDepth = maxDepth;
	}
	
	@Override
	public DefaultQuery<I, D> findCounterExample(A hypothesis,
			Collection<? extends I> inputs) {
		// FIXME: warn about inputs being ignored?
		incrementalWMethodIt.update(hypothesis);
		List<DefaultQuery<I,D>> queryList=new ArrayList<>();
		while(incrementalWMethodIt.hasNext()) {
			Word<I> testCase = incrementalWMethodIt.next();
			
			DefaultQuery<I, D> query = new DefaultQuery<>(testCase);
			queryList.add(query);
			if(queryList.size()>=batchSize || !incrementalWMethodIt.hasNext()){
				oracle.processQueries(queryList);
				for(DefaultQuery<I,D> processedQuery:queryList){
					D hypOut=hypothesis.computeOutput(processedQuery.getInput());
					if(!Objects.equals(processedQuery.getOutput(),hypOut))
						return processedQuery;
				}
				queryList.clear();
			}
		}
		return null;
	}
	
	

}
