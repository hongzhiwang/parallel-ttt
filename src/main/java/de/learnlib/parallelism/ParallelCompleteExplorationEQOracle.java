/* Copyright (C) 2014 TU Dortmund
 * This file is part of LearnLib, http://www.learnlib.de/.
 * 
 * LearnLib is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License version 3.0 as published by the Free Software Foundation.
 * 
 * LearnLib is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with LearnLib; if not, see
 * <http://www.gnu.de/documents/lgpl.en.html>.
 */
package de.learnlib.parallelism;

import de.learnlib.api.EquivalenceOracle;
import de.learnlib.api.MembershipOracle;
import de.learnlib.oracles.DefaultQuery;
import net.automatalib.automata.concepts.DetOutputAutomaton;
import net.automatalib.automata.concepts.OutputAutomaton;
import net.automatalib.automata.fsa.DFA;
import net.automatalib.automata.transout.MealyMachine;
import net.automatalib.commons.util.collections.CollectionsUtil;
import net.automatalib.words.Word;

import javax.annotation.Nullable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Objects;

/**
 * Implements an equivalence check by complete exploration up to a given depth, i.e.,
 * by testing all possible sequences of a certain length within a specified range.
 *
 * @param <I> input symbol type
 * @param <D> output domain type
 * @author Malte Isberner
 */
public class ParallelCompleteExplorationEQOracle<A extends OutputAutomaton<?, I, ?, D>, I, D>
        implements EquivalenceOracle<A, I, D> {

    private int minDepth;
    private int maxDepth;
    private final int batchSize;
    private final MembershipOracle<I, D> sulOracle;
    private A hypothesis;
    private Collection<? extends I> alphabet;

    public static class DFAParallelCompleteExplorationEQQracle<I> extends ParallelCompleteExplorationEQOracle<DFA<?, I>,I,Boolean>
            implements DFAEquivalenceOracle<I>{

        public DFAParallelCompleteExplorationEQQracle(MembershipOracle<I, Boolean> sulOracle, int maxDepth) {
            super(sulOracle, maxDepth);
        }

        public DFAParallelCompleteExplorationEQQracle(MembershipOracle<I, Boolean> sulOracle, int minDepth, int maxDepth, int batchSize) {
            super(sulOracle, minDepth, maxDepth, batchSize);
        }
    }

    public static class MealyParallelCompleteExplorationEQQracle<I,O> extends ParallelCompleteExplorationEQOracle<MealyMachine<?,I,?,O>,I,Word<O>>{

        public MealyParallelCompleteExplorationEQQracle(MembershipOracle<I, Word<O>> sulOracle, int maxDepth) {
            super(sulOracle, maxDepth);
        }

        public MealyParallelCompleteExplorationEQQracle(MembershipOracle<I, Word<O>> sulOracle, int minDepth, int maxDepth, int batchSize) {
            super(sulOracle, minDepth, maxDepth, batchSize);
        }
    }


    /**
     * Constructor.
     *
     * @param sulOracle interface to the system under learning
     * @param maxDepth  maximum exploration depth
     */
    public ParallelCompleteExplorationEQOracle(MembershipOracle<I, D> sulOracle, int maxDepth) {
        this(sulOracle, 1, maxDepth, 1);
    }

    /**
     * Constructor.
     *
     * @param sulOracle interface to the system under learning
     * @param minDepth  minimum exploration depth
     * @param maxDepth  maximum exploration depth
     */
    public ParallelCompleteExplorationEQOracle(MembershipOracle<I, D> sulOracle, int minDepth, int maxDepth, int batchSize) {
        if (maxDepth < minDepth)
            maxDepth = minDepth;

        this.minDepth = minDepth;
        this.maxDepth = maxDepth;

        this.sulOracle = sulOracle;
        this.batchSize = batchSize;
    }

    /*
     * (non-Javadoc)
     * @see de.learnlib.api.EquivalenceOracle#findCounterExample(java.lang.Object, java.util.Collection)
     */
    @Override
    public DefaultQuery<I, D> findCounterExample(A hypothesis,
                                                 Collection<? extends I> alphabet) {
        this.hypothesis = hypothesis;
        this.alphabet = alphabet;
        List<DefaultQuery<I, D>> queryList = new ArrayList<>();
        for (List<? extends I> symList : CollectionsUtil.allTuples(alphabet, minDepth, maxDepth)) {
            Word<I> queryWord = Word.fromList(symList);

            DefaultQuery<I, D> query = new DefaultQuery<>(queryWord);
            queryList.add(query);
            if (queryList.size() >= batchSize) {
                sulOracle.processQueries(queryList);
                for (DefaultQuery<I, D> processedQuery : queryList) {
                    D hypOutput = hypothesis.computeOutput(processedQuery.getInput());
                    if (!Objects.equals(hypOutput, processedQuery.getOutput()))
                        return processedQuery;
                }
                queryList.clear();
            }
        }
        if (queryList.size() > 0) {
            sulOracle.processQueries(queryList);
            for (DefaultQuery<I, D> processedQuery : queryList) {
                D hypOutput = hypothesis.computeOutput(processedQuery.getInput());
                if (!Objects.equals(hypOutput, processedQuery.getOutput()))
                    return processedQuery;
            }
            queryList.clear();
        }
        return null;
    }

}
